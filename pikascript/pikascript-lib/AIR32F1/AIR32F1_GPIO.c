#include "AIR32F1_GPIO.h"

void AIR32F1_GPIO_platformDisable(PikaObj* self) {}
void AIR32F1_GPIO_platformEnable(PikaObj* self) {}
void AIR32F1_GPIO_platformHigh(PikaObj* self) {}
void AIR32F1_GPIO_platformLow(PikaObj* self) {}
void AIR32F1_GPIO_platformRead(PikaObj* self) {}
void AIR32F1_GPIO_platformSetMode(PikaObj* self) {}

const uint32_t GPIO_PA8_EVENT_ID = 0x08;
void AIR32F1_GPIO_platformGetEventId(PikaObj* self) {

}
