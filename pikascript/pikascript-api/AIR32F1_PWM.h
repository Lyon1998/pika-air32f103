/* ******************************** */
/* Warning! Don't modify this file! */
/* ******************************** */
#ifndef __AIR32F1_PWM__H
#define __AIR32F1_PWM__H
#include <stdio.h>
#include <stdlib.h>
#include "PikaObj.h"

PikaObj *New_AIR32F1_PWM(Args *args);

void AIR32F1_PWM_platformDisable(PikaObj *self);
void AIR32F1_PWM_platformEnable(PikaObj *self);
void AIR32F1_PWM_platformGetEventId(PikaObj *self);
void AIR32F1_PWM_platformSetDuty(PikaObj *self);
void AIR32F1_PWM_platformSetFrequency(PikaObj *self);

#endif
