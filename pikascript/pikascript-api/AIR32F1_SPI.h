/* ******************************** */
/* Warning! Don't modify this file! */
/* ******************************** */
#ifndef __AIR32F1_SPI__H
#define __AIR32F1_SPI__H
#include <stdio.h>
#include <stdlib.h>
#include "PikaObj.h"

PikaObj *New_AIR32F1_SPI(Args *args);

void AIR32F1_SPI_platformDisable(PikaObj *self);
void AIR32F1_SPI_platformEnable(PikaObj *self);
void AIR32F1_SPI_platformGetEventId(PikaObj *self);
void AIR32F1_SPI_platformRead(PikaObj *self);
void AIR32F1_SPI_platformReadBytes(PikaObj *self);
void AIR32F1_SPI_platformWrite(PikaObj *self);
void AIR32F1_SPI_platformWriteBytes(PikaObj *self);

#endif
