/* ******************************** */
/* Warning! Don't modify this file! */
/* ******************************** */
#include <stdio.h>
#include <stdlib.h>
#include "BaseObj.h"
#include "AIR32F1.h"
#include "TinyObj.h"
#include "AIR32F1_ADC.h"
#include "PikaStdDevice_ADC.h"
#include "AIR32F1_CAN.h"
#include "PikaStdDevice_CAN.h"
#include "AIR32F1_GPIO.h"
#include "PikaStdDevice_GPIO.h"
#include "AIR32F1_IIC.h"
#include "PikaStdDevice_IIC.h"
#include "AIR32F1_PWM.h"
#include "PikaStdDevice_PWM.h"
#include "AIR32F1_SPI.h"
#include "PikaStdDevice_SPI.h"
#include "AIR32F1_Time.h"
#include "PikaStdDevice_Time.h"
#include "AIR32F1_UART.h"
#include "PikaStdDevice_UART.h"
#include "PikaDebug.h"
#include "TinyObj.h"
#include "PikaDebug_Debuger.h"
#include "TinyObj.h"
#include "PikaMain.h"
#include "PikaStdLib_SysObj.h"
#include "AIR32F1.h"
#include "PikaStdLib.h"
#include "PikaStdData.h"
#include "TinyObj.h"
#include "PikaStdData_ByteArray.h"
#include "TinyObj.h"
#include "PikaStdData_Dict.h"
#include "TinyObj.h"
#include "PikaStdData_FILEIO.h"
#include "TinyObj.h"
#include "PikaStdData_List.h"
#include "PikaStdData_Tuple.h"
#include "PikaStdData_String.h"
#include "TinyObj.h"
#include "PikaStdData_Tuple.h"
#include "TinyObj.h"
#include "PikaStdData_Utils.h"
#include "TinyObj.h"
#include "PikaStdData_dict_keys.h"
#include "TinyObj.h"
#include "PikaStdDevice.h"
#include "TinyObj.h"
#include "PikaStdDevice_ADC.h"
#include "PikaStdDevice_BaseDev.h"
#include "PikaStdDevice_BaseDev.h"
#include "TinyObj.h"
#include "PikaStdDevice_CAN.h"
#include "PikaStdDevice_BaseDev.h"
#include "PikaStdDevice_GPIO.h"
#include "PikaStdDevice_BaseDev.h"
#include "PikaStdDevice_IIC.h"
#include "PikaStdDevice_BaseDev.h"
#include "PikaStdDevice_PWM.h"
#include "PikaStdDevice_BaseDev.h"
#include "PikaStdDevice_SPI.h"
#include "PikaStdDevice_BaseDev.h"
#include "PikaStdDevice_Time.h"
#include "PikaStdDevice_BaseDev.h"
#include "PikaStdDevice_UART.h"
#include "PikaStdDevice_BaseDev.h"
#include "PikaStdLib.h"
#include "TinyObj.h"
#include "PikaStdLib_MemChecker.h"
#include "TinyObj.h"
#include "PikaStdLib_RangeObj.h"
#include "TinyObj.h"
#include "PikaStdLib_StringObj.h"
#include "TinyObj.h"
#include "PikaStdLib_SysObj.h"
#include "TinyObj.h"
#include "PikaStdTask.h"
#include "TinyObj.h"
#include "PikaStdTask_Task.h"
#include "PikaStdLib_SysObj.h"
#include "PikaStdData_List.h"

void AIR32F1_ADCMethod(PikaObj *self, Args *args){
    Arg* res = AIR32F1_ADC(self);
    method_returnArg(args, res);
}

void AIR32F1_CANMethod(PikaObj *self, Args *args){
    Arg* res = AIR32F1_CAN(self);
    method_returnArg(args, res);
}

void AIR32F1_GPIOMethod(PikaObj *self, Args *args){
    Arg* res = AIR32F1_GPIO(self);
    method_returnArg(args, res);
}

void AIR32F1_IICMethod(PikaObj *self, Args *args){
    Arg* res = AIR32F1_IIC(self);
    method_returnArg(args, res);
}

void AIR32F1_PWMMethod(PikaObj *self, Args *args){
    Arg* res = AIR32F1_PWM(self);
    method_returnArg(args, res);
}

void AIR32F1_SPIMethod(PikaObj *self, Args *args){
    Arg* res = AIR32F1_SPI(self);
    method_returnArg(args, res);
}

void AIR32F1_TimeMethod(PikaObj *self, Args *args){
    Arg* res = AIR32F1_Time(self);
    method_returnArg(args, res);
}

void AIR32F1_UARTMethod(PikaObj *self, Args *args){
    Arg* res = AIR32F1_UART(self);
    method_returnArg(args, res);
}

PikaObj *New_AIR32F1(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineConstructor(self, "ADC()->any", AIR32F1_ADCMethod);
    class_defineConstructor(self, "CAN()->any", AIR32F1_CANMethod);
    class_defineConstructor(self, "GPIO()->any", AIR32F1_GPIOMethod);
    class_defineConstructor(self, "IIC()->any", AIR32F1_IICMethod);
    class_defineConstructor(self, "PWM()->any", AIR32F1_PWMMethod);
    class_defineConstructor(self, "SPI()->any", AIR32F1_SPIMethod);
    class_defineConstructor(self, "Time()->any", AIR32F1_TimeMethod);
    class_defineConstructor(self, "UART()->any", AIR32F1_UARTMethod);
    return self;
}

void AIR32F1_ADC_platformDisableMethod(PikaObj *self, Args *args){
    AIR32F1_ADC_platformDisable(self);
}

void AIR32F1_ADC_platformEnableMethod(PikaObj *self, Args *args){
    AIR32F1_ADC_platformEnable(self);
}

void AIR32F1_ADC_platformGetEventIdMethod(PikaObj *self, Args *args){
    AIR32F1_ADC_platformGetEventId(self);
}

void AIR32F1_ADC_platformReadMethod(PikaObj *self, Args *args){
    AIR32F1_ADC_platformRead(self);
}

PikaObj *New_AIR32F1_ADC(Args *args){
    PikaObj *self = New_PikaStdDevice_ADC(args);
    class_defineMethod(self, "platformDisable()", AIR32F1_ADC_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", AIR32F1_ADC_platformEnableMethod);
    class_defineMethod(self, "platformGetEventId()", AIR32F1_ADC_platformGetEventIdMethod);
    class_defineMethod(self, "platformRead()", AIR32F1_ADC_platformReadMethod);
    return self;
}

Arg *AIR32F1_ADC(PikaObj *self){
    return obj_newObjInPackage(New_AIR32F1_ADC);
}
void AIR32F1_CAN_platformDisableMethod(PikaObj *self, Args *args){
    AIR32F1_CAN_platformDisable(self);
}

void AIR32F1_CAN_platformEnableMethod(PikaObj *self, Args *args){
    AIR32F1_CAN_platformEnable(self);
}

void AIR32F1_CAN_platformGetEventIdMethod(PikaObj *self, Args *args){
    AIR32F1_CAN_platformGetEventId(self);
}

void AIR32F1_CAN_platformReadMethod(PikaObj *self, Args *args){
    AIR32F1_CAN_platformRead(self);
}

void AIR32F1_CAN_platformReadBytesMethod(PikaObj *self, Args *args){
    AIR32F1_CAN_platformReadBytes(self);
}

void AIR32F1_CAN_platformWriteMethod(PikaObj *self, Args *args){
    AIR32F1_CAN_platformWrite(self);
}

void AIR32F1_CAN_platformWriteBytesMethod(PikaObj *self, Args *args){
    AIR32F1_CAN_platformWriteBytes(self);
}

PikaObj *New_AIR32F1_CAN(Args *args){
    PikaObj *self = New_PikaStdDevice_CAN(args);
    class_defineMethod(self, "platformDisable()", AIR32F1_CAN_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", AIR32F1_CAN_platformEnableMethod);
    class_defineMethod(self, "platformGetEventId()", AIR32F1_CAN_platformGetEventIdMethod);
    class_defineMethod(self, "platformRead()", AIR32F1_CAN_platformReadMethod);
    class_defineMethod(self, "platformReadBytes()", AIR32F1_CAN_platformReadBytesMethod);
    class_defineMethod(self, "platformWrite()", AIR32F1_CAN_platformWriteMethod);
    class_defineMethod(self, "platformWriteBytes()", AIR32F1_CAN_platformWriteBytesMethod);
    return self;
}

Arg *AIR32F1_CAN(PikaObj *self){
    return obj_newObjInPackage(New_AIR32F1_CAN);
}
void AIR32F1_GPIO_platformDisableMethod(PikaObj *self, Args *args){
    AIR32F1_GPIO_platformDisable(self);
}

void AIR32F1_GPIO_platformEnableMethod(PikaObj *self, Args *args){
    AIR32F1_GPIO_platformEnable(self);
}

void AIR32F1_GPIO_platformGetEventIdMethod(PikaObj *self, Args *args){
    AIR32F1_GPIO_platformGetEventId(self);
}

void AIR32F1_GPIO_platformHighMethod(PikaObj *self, Args *args){
    AIR32F1_GPIO_platformHigh(self);
}

void AIR32F1_GPIO_platformLowMethod(PikaObj *self, Args *args){
    AIR32F1_GPIO_platformLow(self);
}

void AIR32F1_GPIO_platformReadMethod(PikaObj *self, Args *args){
    AIR32F1_GPIO_platformRead(self);
}

void AIR32F1_GPIO_platformSetModeMethod(PikaObj *self, Args *args){
    AIR32F1_GPIO_platformSetMode(self);
}

PikaObj *New_AIR32F1_GPIO(Args *args){
    PikaObj *self = New_PikaStdDevice_GPIO(args);
    class_defineMethod(self, "platformDisable()", AIR32F1_GPIO_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", AIR32F1_GPIO_platformEnableMethod);
    class_defineMethod(self, "platformGetEventId()", AIR32F1_GPIO_platformGetEventIdMethod);
    class_defineMethod(self, "platformHigh()", AIR32F1_GPIO_platformHighMethod);
    class_defineMethod(self, "platformLow()", AIR32F1_GPIO_platformLowMethod);
    class_defineMethod(self, "platformRead()", AIR32F1_GPIO_platformReadMethod);
    class_defineMethod(self, "platformSetMode()", AIR32F1_GPIO_platformSetModeMethod);
    return self;
}

Arg *AIR32F1_GPIO(PikaObj *self){
    return obj_newObjInPackage(New_AIR32F1_GPIO);
}
void AIR32F1_IIC_platformDisableMethod(PikaObj *self, Args *args){
    AIR32F1_IIC_platformDisable(self);
}

void AIR32F1_IIC_platformEnableMethod(PikaObj *self, Args *args){
    AIR32F1_IIC_platformEnable(self);
}

void AIR32F1_IIC_platformGetEventIdMethod(PikaObj *self, Args *args){
    AIR32F1_IIC_platformGetEventId(self);
}

void AIR32F1_IIC_platformReadMethod(PikaObj *self, Args *args){
    AIR32F1_IIC_platformRead(self);
}

void AIR32F1_IIC_platformReadBytesMethod(PikaObj *self, Args *args){
    AIR32F1_IIC_platformReadBytes(self);
}

void AIR32F1_IIC_platformWriteMethod(PikaObj *self, Args *args){
    AIR32F1_IIC_platformWrite(self);
}

void AIR32F1_IIC_platformWriteBytesMethod(PikaObj *self, Args *args){
    AIR32F1_IIC_platformWriteBytes(self);
}

PikaObj *New_AIR32F1_IIC(Args *args){
    PikaObj *self = New_PikaStdDevice_IIC(args);
    class_defineMethod(self, "platformDisable()", AIR32F1_IIC_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", AIR32F1_IIC_platformEnableMethod);
    class_defineMethod(self, "platformGetEventId()", AIR32F1_IIC_platformGetEventIdMethod);
    class_defineMethod(self, "platformRead()", AIR32F1_IIC_platformReadMethod);
    class_defineMethod(self, "platformReadBytes()", AIR32F1_IIC_platformReadBytesMethod);
    class_defineMethod(self, "platformWrite()", AIR32F1_IIC_platformWriteMethod);
    class_defineMethod(self, "platformWriteBytes()", AIR32F1_IIC_platformWriteBytesMethod);
    return self;
}

Arg *AIR32F1_IIC(PikaObj *self){
    return obj_newObjInPackage(New_AIR32F1_IIC);
}
void AIR32F1_PWM_platformDisableMethod(PikaObj *self, Args *args){
    AIR32F1_PWM_platformDisable(self);
}

void AIR32F1_PWM_platformEnableMethod(PikaObj *self, Args *args){
    AIR32F1_PWM_platformEnable(self);
}

void AIR32F1_PWM_platformGetEventIdMethod(PikaObj *self, Args *args){
    AIR32F1_PWM_platformGetEventId(self);
}

void AIR32F1_PWM_platformSetDutyMethod(PikaObj *self, Args *args){
    AIR32F1_PWM_platformSetDuty(self);
}

void AIR32F1_PWM_platformSetFrequencyMethod(PikaObj *self, Args *args){
    AIR32F1_PWM_platformSetFrequency(self);
}

PikaObj *New_AIR32F1_PWM(Args *args){
    PikaObj *self = New_PikaStdDevice_PWM(args);
    class_defineMethod(self, "platformDisable()", AIR32F1_PWM_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", AIR32F1_PWM_platformEnableMethod);
    class_defineMethod(self, "platformGetEventId()", AIR32F1_PWM_platformGetEventIdMethod);
    class_defineMethod(self, "platformSetDuty()", AIR32F1_PWM_platformSetDutyMethod);
    class_defineMethod(self, "platformSetFrequency()", AIR32F1_PWM_platformSetFrequencyMethod);
    return self;
}

Arg *AIR32F1_PWM(PikaObj *self){
    return obj_newObjInPackage(New_AIR32F1_PWM);
}
void AIR32F1_SPI_platformDisableMethod(PikaObj *self, Args *args){
    AIR32F1_SPI_platformDisable(self);
}

void AIR32F1_SPI_platformEnableMethod(PikaObj *self, Args *args){
    AIR32F1_SPI_platformEnable(self);
}

void AIR32F1_SPI_platformGetEventIdMethod(PikaObj *self, Args *args){
    AIR32F1_SPI_platformGetEventId(self);
}

void AIR32F1_SPI_platformReadMethod(PikaObj *self, Args *args){
    AIR32F1_SPI_platformRead(self);
}

void AIR32F1_SPI_platformReadBytesMethod(PikaObj *self, Args *args){
    AIR32F1_SPI_platformReadBytes(self);
}

void AIR32F1_SPI_platformWriteMethod(PikaObj *self, Args *args){
    AIR32F1_SPI_platformWrite(self);
}

void AIR32F1_SPI_platformWriteBytesMethod(PikaObj *self, Args *args){
    AIR32F1_SPI_platformWriteBytes(self);
}

PikaObj *New_AIR32F1_SPI(Args *args){
    PikaObj *self = New_PikaStdDevice_SPI(args);
    class_defineMethod(self, "platformDisable()", AIR32F1_SPI_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", AIR32F1_SPI_platformEnableMethod);
    class_defineMethod(self, "platformGetEventId()", AIR32F1_SPI_platformGetEventIdMethod);
    class_defineMethod(self, "platformRead()", AIR32F1_SPI_platformReadMethod);
    class_defineMethod(self, "platformReadBytes()", AIR32F1_SPI_platformReadBytesMethod);
    class_defineMethod(self, "platformWrite()", AIR32F1_SPI_platformWriteMethod);
    class_defineMethod(self, "platformWriteBytes()", AIR32F1_SPI_platformWriteBytesMethod);
    return self;
}

Arg *AIR32F1_SPI(PikaObj *self){
    return obj_newObjInPackage(New_AIR32F1_SPI);
}
void AIR32F1_Time_platformGetEventIdMethod(PikaObj *self, Args *args){
    AIR32F1_Time_platformGetEventId(self);
}

void AIR32F1_Time_platformGetTickMethod(PikaObj *self, Args *args){
    AIR32F1_Time_platformGetTick(self);
}

void AIR32F1_Time_sleep_msMethod(PikaObj *self, Args *args){
    int ms = args_getInt(args, "ms");
    AIR32F1_Time_sleep_ms(self, ms);
}

void AIR32F1_Time_sleep_sMethod(PikaObj *self, Args *args){
    int s = args_getInt(args, "s");
    AIR32F1_Time_sleep_s(self, s);
}

PikaObj *New_AIR32F1_Time(Args *args){
    PikaObj *self = New_PikaStdDevice_Time(args);
    class_defineMethod(self, "platformGetEventId()", AIR32F1_Time_platformGetEventIdMethod);
    class_defineMethod(self, "platformGetTick()", AIR32F1_Time_platformGetTickMethod);
    class_defineMethod(self, "sleep_ms(ms:int)", AIR32F1_Time_sleep_msMethod);
    class_defineMethod(self, "sleep_s(s:int)", AIR32F1_Time_sleep_sMethod);
    return self;
}

Arg *AIR32F1_Time(PikaObj *self){
    return obj_newObjInPackage(New_AIR32F1_Time);
}
void AIR32F1_UART_platformDisableMethod(PikaObj *self, Args *args){
    AIR32F1_UART_platformDisable(self);
}

void AIR32F1_UART_platformEnableMethod(PikaObj *self, Args *args){
    AIR32F1_UART_platformEnable(self);
}

void AIR32F1_UART_platformGetEventIdMethod(PikaObj *self, Args *args){
    AIR32F1_UART_platformGetEventId(self);
}

void AIR32F1_UART_platformReadMethod(PikaObj *self, Args *args){
    AIR32F1_UART_platformRead(self);
}

void AIR32F1_UART_platformReadBytesMethod(PikaObj *self, Args *args){
    AIR32F1_UART_platformReadBytes(self);
}

void AIR32F1_UART_platformWriteMethod(PikaObj *self, Args *args){
    AIR32F1_UART_platformWrite(self);
}

void AIR32F1_UART_platformWriteBytesMethod(PikaObj *self, Args *args){
    AIR32F1_UART_platformWriteBytes(self);
}

PikaObj *New_AIR32F1_UART(Args *args){
    PikaObj *self = New_PikaStdDevice_UART(args);
    class_defineMethod(self, "platformDisable()", AIR32F1_UART_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", AIR32F1_UART_platformEnableMethod);
    class_defineMethod(self, "platformGetEventId()", AIR32F1_UART_platformGetEventIdMethod);
    class_defineMethod(self, "platformRead()", AIR32F1_UART_platformReadMethod);
    class_defineMethod(self, "platformReadBytes()", AIR32F1_UART_platformReadBytesMethod);
    class_defineMethod(self, "platformWrite()", AIR32F1_UART_platformWriteMethod);
    class_defineMethod(self, "platformWriteBytes()", AIR32F1_UART_platformWriteBytesMethod);
    return self;
}

Arg *AIR32F1_UART(PikaObj *self){
    return obj_newObjInPackage(New_AIR32F1_UART);
}
void PikaDebug_DebugerMethod(PikaObj *self, Args *args){
    Arg* res = PikaDebug_Debuger(self);
    method_returnArg(args, res);
}

PikaObj *New_PikaDebug(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineConstructor(self, "Debuger()->any", PikaDebug_DebugerMethod);
    return self;
}

void PikaDebug_Debuger___init__Method(PikaObj *self, Args *args){
    PikaDebug_Debuger___init__(self);
}

void PikaDebug_Debuger_set_traceMethod(PikaObj *self, Args *args){
    PikaDebug_Debuger_set_trace(self);
}

PikaObj *New_PikaDebug_Debuger(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineMethod(self, "__init__()", PikaDebug_Debuger___init__Method);
    class_defineMethod(self, "set_trace()", PikaDebug_Debuger_set_traceMethod);
    return self;
}

Arg *PikaDebug_Debuger(PikaObj *self){
    return obj_newObjInPackage(New_PikaDebug_Debuger);
}
PikaObj *New_PikaMain(Args *args){
    PikaObj *self = New_PikaStdLib_SysObj(args);
    obj_newObj(self, "AIR32F1", "AIR32F1", New_AIR32F1);
    obj_newObj(self, "PikaStdLib", "PikaStdLib", New_PikaStdLib);
    return self;
}

Arg *PikaMain(PikaObj *self){
    return obj_newObjInPackage(New_PikaMain);
}
void PikaStdData_ByteArrayMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdData_ByteArray(self);
    method_returnArg(args, res);
}

void PikaStdData_DictMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdData_Dict(self);
    method_returnArg(args, res);
}

void PikaStdData_FILEIOMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdData_FILEIO(self);
    method_returnArg(args, res);
}

void PikaStdData_ListMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdData_List(self);
    method_returnArg(args, res);
}

void PikaStdData_StringMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdData_String(self);
    method_returnArg(args, res);
}

void PikaStdData_TupleMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdData_Tuple(self);
    method_returnArg(args, res);
}

void PikaStdData_UtilsMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdData_Utils(self);
    method_returnArg(args, res);
}

void PikaStdData_dict_keysMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdData_dict_keys(self);
    method_returnArg(args, res);
}

PikaObj *New_PikaStdData(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineConstructor(self, "ByteArray()->any", PikaStdData_ByteArrayMethod);
    class_defineConstructor(self, "Dict()->any", PikaStdData_DictMethod);
    class_defineConstructor(self, "FILEIO()->any", PikaStdData_FILEIOMethod);
    class_defineConstructor(self, "List()->any", PikaStdData_ListMethod);
    class_defineConstructor(self, "String()->any", PikaStdData_StringMethod);
    class_defineConstructor(self, "Tuple()->any", PikaStdData_TupleMethod);
    class_defineConstructor(self, "Utils()->any", PikaStdData_UtilsMethod);
    class_defineConstructor(self, "dict_keys()->any", PikaStdData_dict_keysMethod);
    return self;
}

void PikaStdData_ByteArray___getitem__Method(PikaObj *self, Args *args){
    int __key = args_getInt(args, "__key");
    int res = PikaStdData_ByteArray___getitem__(self, __key);
    method_returnInt(args, res);
}

void PikaStdData_ByteArray___init__Method(PikaObj *self, Args *args){
    Arg* bytes = args_getArg(args, "bytes");
    PikaStdData_ByteArray___init__(self, bytes);
}

void PikaStdData_ByteArray___iter__Method(PikaObj *self, Args *args){
    Arg* res = PikaStdData_ByteArray___iter__(self);
    method_returnArg(args, res);
}

void PikaStdData_ByteArray___next__Method(PikaObj *self, Args *args){
    Arg* res = PikaStdData_ByteArray___next__(self);
    method_returnArg(args, res);
}

void PikaStdData_ByteArray___setitem__Method(PikaObj *self, Args *args){
    int __key = args_getInt(args, "__key");
    int __val = args_getInt(args, "__val");
    PikaStdData_ByteArray___setitem__(self, __key, __val);
}

void PikaStdData_ByteArray___str__Method(PikaObj *self, Args *args){
    char* res = PikaStdData_ByteArray___str__(self);
    method_returnStr(args, res);
}

void PikaStdData_ByteArray_decodeMethod(PikaObj *self, Args *args){
    char* res = PikaStdData_ByteArray_decode(self);
    method_returnStr(args, res);
}

PikaObj *New_PikaStdData_ByteArray(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineMethod(self, "__getitem__(__key:int)->int", PikaStdData_ByteArray___getitem__Method);
    class_defineMethod(self, "__init__(bytes:any)", PikaStdData_ByteArray___init__Method);
    class_defineMethod(self, "__iter__()->any", PikaStdData_ByteArray___iter__Method);
    class_defineMethod(self, "__next__()->any", PikaStdData_ByteArray___next__Method);
    class_defineMethod(self, "__setitem__(__key:int,__val:int)", PikaStdData_ByteArray___setitem__Method);
    class_defineMethod(self, "__str__()->str", PikaStdData_ByteArray___str__Method);
    class_defineMethod(self, "decode()->str", PikaStdData_ByteArray_decodeMethod);
    return self;
}

Arg *PikaStdData_ByteArray(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdData_ByteArray);
}
void PikaStdData_Dict___del__Method(PikaObj *self, Args *args){
    PikaStdData_Dict___del__(self);
}

void PikaStdData_Dict___getitem__Method(PikaObj *self, Args *args){
    Arg* __key = args_getArg(args, "__key");
    Arg* res = PikaStdData_Dict___getitem__(self, __key);
    method_returnArg(args, res);
}

void PikaStdData_Dict___init__Method(PikaObj *self, Args *args){
    PikaStdData_Dict___init__(self);
}

void PikaStdData_Dict___iter__Method(PikaObj *self, Args *args){
    Arg* res = PikaStdData_Dict___iter__(self);
    method_returnArg(args, res);
}

void PikaStdData_Dict___len__Method(PikaObj *self, Args *args){
    int res = PikaStdData_Dict___len__(self);
    method_returnInt(args, res);
}

void PikaStdData_Dict___next__Method(PikaObj *self, Args *args){
    Arg* res = PikaStdData_Dict___next__(self);
    method_returnArg(args, res);
}

void PikaStdData_Dict___setitem__Method(PikaObj *self, Args *args){
    Arg* __key = args_getArg(args, "__key");
    Arg* __val = args_getArg(args, "__val");
    PikaStdData_Dict___setitem__(self, __key, __val);
}

void PikaStdData_Dict___str__Method(PikaObj *self, Args *args){
    char* res = PikaStdData_Dict___str__(self);
    method_returnStr(args, res);
}

void PikaStdData_Dict_getMethod(PikaObj *self, Args *args){
    char* key = args_getStr(args, "key");
    Arg* res = PikaStdData_Dict_get(self, key);
    method_returnArg(args, res);
}

void PikaStdData_Dict_keysMethod(PikaObj *self, Args *args){
    PikaObj* res = PikaStdData_Dict_keys(self);
    method_returnObj(args, res);
}

void PikaStdData_Dict_removeMethod(PikaObj *self, Args *args){
    char* key = args_getStr(args, "key");
    PikaStdData_Dict_remove(self, key);
}

void PikaStdData_Dict_setMethod(PikaObj *self, Args *args){
    char* key = args_getStr(args, "key");
    Arg* arg = args_getArg(args, "arg");
    PikaStdData_Dict_set(self, key, arg);
}

PikaObj *New_PikaStdData_Dict(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineMethod(self, "__del__()", PikaStdData_Dict___del__Method);
    class_defineMethod(self, "__getitem__(__key:any)->any", PikaStdData_Dict___getitem__Method);
    class_defineMethod(self, "__init__()", PikaStdData_Dict___init__Method);
    class_defineMethod(self, "__iter__()->any", PikaStdData_Dict___iter__Method);
    class_defineMethod(self, "__len__()->int", PikaStdData_Dict___len__Method);
    class_defineMethod(self, "__next__()->any", PikaStdData_Dict___next__Method);
    class_defineMethod(self, "__setitem__(__key:any,__val:any)", PikaStdData_Dict___setitem__Method);
    class_defineMethod(self, "__str__()->str", PikaStdData_Dict___str__Method);
    class_defineMethod(self, "get(key:str)->any", PikaStdData_Dict_getMethod);
    class_defineMethod(self, "keys()->dict_keys", PikaStdData_Dict_keysMethod);
    class_defineMethod(self, "remove(key:str)", PikaStdData_Dict_removeMethod);
    class_defineMethod(self, "set(key:str,arg:any)", PikaStdData_Dict_setMethod);
    return self;
}

Arg *PikaStdData_Dict(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdData_Dict);
}
void PikaStdData_FILEIO_closeMethod(PikaObj *self, Args *args){
    PikaStdData_FILEIO_close(self);
}

void PikaStdData_FILEIO_initMethod(PikaObj *self, Args *args){
    char* path = args_getStr(args, "path");
    char* mode = args_getStr(args, "mode");
    int res = PikaStdData_FILEIO_init(self, path, mode);
    method_returnInt(args, res);
}

void PikaStdData_FILEIO_readMethod(PikaObj *self, Args *args){
    int size = args_getInt(args, "size");
    Arg* res = PikaStdData_FILEIO_read(self, size);
    method_returnArg(args, res);
}

void PikaStdData_FILEIO_readlineMethod(PikaObj *self, Args *args){
    char* res = PikaStdData_FILEIO_readline(self);
    method_returnStr(args, res);
}

void PikaStdData_FILEIO_readlinesMethod(PikaObj *self, Args *args){
    PikaObj* res = PikaStdData_FILEIO_readlines(self);
    method_returnObj(args, res);
}

void PikaStdData_FILEIO_seekMethod(PikaObj *self, Args *args){
    int offset = args_getInt(args, "offset");
    PikaTuple* fromwhere = args_getTuple(args, "fromwhere");
    int res = PikaStdData_FILEIO_seek(self, offset, fromwhere);
    method_returnInt(args, res);
}

void PikaStdData_FILEIO_tellMethod(PikaObj *self, Args *args){
    int res = PikaStdData_FILEIO_tell(self);
    method_returnInt(args, res);
}

void PikaStdData_FILEIO_writeMethod(PikaObj *self, Args *args){
    Arg* s = args_getArg(args, "s");
    int res = PikaStdData_FILEIO_write(self, s);
    method_returnInt(args, res);
}

void PikaStdData_FILEIO_writelinesMethod(PikaObj *self, Args *args){
    PikaObj* lines = args_getPtr(args, "lines");
    PikaStdData_FILEIO_writelines(self, lines);
}

PikaObj *New_PikaStdData_FILEIO(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineMethod(self, "close()", PikaStdData_FILEIO_closeMethod);
    class_defineMethod(self, "init(path:str,mode:str)->int", PikaStdData_FILEIO_initMethod);
    class_defineMethod(self, "read(size:int)->any", PikaStdData_FILEIO_readMethod);
    class_defineMethod(self, "readline()->str", PikaStdData_FILEIO_readlineMethod);
    class_defineMethod(self, "readlines()->List", PikaStdData_FILEIO_readlinesMethod);
    class_defineMethod(self, "seek(offset:int,*fromwhere)->int", PikaStdData_FILEIO_seekMethod);
    class_defineMethod(self, "tell()->int", PikaStdData_FILEIO_tellMethod);
    class_defineMethod(self, "write(s:any)->int", PikaStdData_FILEIO_writeMethod);
    class_defineMethod(self, "writelines(lines:List)", PikaStdData_FILEIO_writelinesMethod);
    return self;
}

Arg *PikaStdData_FILEIO(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdData_FILEIO);
}
void PikaStdData_List___add__Method(PikaObj *self, Args *args){
    PikaObj* others = args_getPtr(args, "others");
    PikaObj* res = PikaStdData_List___add__(self, others);
    method_returnObj(args, res);
}

void PikaStdData_List___init__Method(PikaObj *self, Args *args){
    PikaStdData_List___init__(self);
}

void PikaStdData_List___setitem__Method(PikaObj *self, Args *args){
    Arg* __key = args_getArg(args, "__key");
    Arg* __val = args_getArg(args, "__val");
    PikaStdData_List___setitem__(self, __key, __val);
}

void PikaStdData_List___str__Method(PikaObj *self, Args *args){
    char* res = PikaStdData_List___str__(self);
    method_returnStr(args, res);
}

void PikaStdData_List_appendMethod(PikaObj *self, Args *args){
    Arg* arg = args_getArg(args, "arg");
    PikaStdData_List_append(self, arg);
}

void PikaStdData_List_reverseMethod(PikaObj *self, Args *args){
    PikaStdData_List_reverse(self);
}

void PikaStdData_List_setMethod(PikaObj *self, Args *args){
    int i = args_getInt(args, "i");
    Arg* arg = args_getArg(args, "arg");
    PikaStdData_List_set(self, i, arg);
}

PikaObj *New_PikaStdData_List(Args *args){
    PikaObj *self = New_PikaStdData_Tuple(args);
    class_defineMethod(self, "__add__(others:List)->List", PikaStdData_List___add__Method);
    class_defineMethod(self, "__init__()", PikaStdData_List___init__Method);
    class_defineMethod(self, "__setitem__(__key:any,__val:any)", PikaStdData_List___setitem__Method);
    class_defineMethod(self, "__str__()->str", PikaStdData_List___str__Method);
    class_defineMethod(self, "append(arg:any)", PikaStdData_List_appendMethod);
    class_defineMethod(self, "reverse()", PikaStdData_List_reverseMethod);
    class_defineMethod(self, "set(i:int,arg:any)", PikaStdData_List_setMethod);
    return self;
}

Arg *PikaStdData_List(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdData_List);
}
void PikaStdData_String___getitem__Method(PikaObj *self, Args *args){
    Arg* __key = args_getArg(args, "__key");
    Arg* res = PikaStdData_String___getitem__(self, __key);
    method_returnArg(args, res);
}

void PikaStdData_String___init__Method(PikaObj *self, Args *args){
    char* s = args_getStr(args, "s");
    PikaStdData_String___init__(self, s);
}

void PikaStdData_String___iter__Method(PikaObj *self, Args *args){
    Arg* res = PikaStdData_String___iter__(self);
    method_returnArg(args, res);
}

void PikaStdData_String___len__Method(PikaObj *self, Args *args){
    int res = PikaStdData_String___len__(self);
    method_returnInt(args, res);
}

void PikaStdData_String___next__Method(PikaObj *self, Args *args){
    Arg* res = PikaStdData_String___next__(self);
    method_returnArg(args, res);
}

void PikaStdData_String___setitem__Method(PikaObj *self, Args *args){
    Arg* __key = args_getArg(args, "__key");
    Arg* __val = args_getArg(args, "__val");
    PikaStdData_String___setitem__(self, __key, __val);
}

void PikaStdData_String___str__Method(PikaObj *self, Args *args){
    char* res = PikaStdData_String___str__(self);
    method_returnStr(args, res);
}

void PikaStdData_String_encodeMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdData_String_encode(self);
    method_returnArg(args, res);
}

void PikaStdData_String_endswithMethod(PikaObj *self, Args *args){
    char* suffix = args_getStr(args, "suffix");
    int res = PikaStdData_String_endswith(self, suffix);
    method_returnInt(args, res);
}

void PikaStdData_String_getMethod(PikaObj *self, Args *args){
    char* res = PikaStdData_String_get(self);
    method_returnStr(args, res);
}

void PikaStdData_String_isalnumMethod(PikaObj *self, Args *args){
    int res = PikaStdData_String_isalnum(self);
    method_returnInt(args, res);
}

void PikaStdData_String_isalphaMethod(PikaObj *self, Args *args){
    int res = PikaStdData_String_isalpha(self);
    method_returnInt(args, res);
}

void PikaStdData_String_isdigitMethod(PikaObj *self, Args *args){
    int res = PikaStdData_String_isdigit(self);
    method_returnInt(args, res);
}

void PikaStdData_String_islowerMethod(PikaObj *self, Args *args){
    int res = PikaStdData_String_islower(self);
    method_returnInt(args, res);
}

void PikaStdData_String_isspaceMethod(PikaObj *self, Args *args){
    int res = PikaStdData_String_isspace(self);
    method_returnInt(args, res);
}

void PikaStdData_String_replaceMethod(PikaObj *self, Args *args){
    char* old = args_getStr(args, "old");
    char* new = args_getStr(args, "new");
    char* res = PikaStdData_String_replace(self, old, new);
    method_returnStr(args, res);
}

void PikaStdData_String_setMethod(PikaObj *self, Args *args){
    char* s = args_getStr(args, "s");
    PikaStdData_String_set(self, s);
}

void PikaStdData_String_splitMethod(PikaObj *self, Args *args){
    char* s = args_getStr(args, "s");
    PikaObj* res = PikaStdData_String_split(self, s);
    method_returnObj(args, res);
}

void PikaStdData_String_startswithMethod(PikaObj *self, Args *args){
    char* prefix = args_getStr(args, "prefix");
    int res = PikaStdData_String_startswith(self, prefix);
    method_returnInt(args, res);
}

void PikaStdData_String_stripMethod(PikaObj *self, Args *args){
    char* res = PikaStdData_String_strip(self);
    method_returnStr(args, res);
}

PikaObj *New_PikaStdData_String(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineMethod(self, "__getitem__(__key:any)->any", PikaStdData_String___getitem__Method);
    class_defineMethod(self, "__init__(s:str)", PikaStdData_String___init__Method);
    class_defineMethod(self, "__iter__()->any", PikaStdData_String___iter__Method);
    class_defineMethod(self, "__len__()->int", PikaStdData_String___len__Method);
    class_defineMethod(self, "__next__()->any", PikaStdData_String___next__Method);
    class_defineMethod(self, "__setitem__(__key:any,__val:any)", PikaStdData_String___setitem__Method);
    class_defineMethod(self, "__str__()->str", PikaStdData_String___str__Method);
    class_defineMethod(self, "encode()->bytes", PikaStdData_String_encodeMethod);
    class_defineMethod(self, "endswith(suffix:str)->int", PikaStdData_String_endswithMethod);
    class_defineMethod(self, "get()->str", PikaStdData_String_getMethod);
    class_defineMethod(self, "isalnum()->int", PikaStdData_String_isalnumMethod);
    class_defineMethod(self, "isalpha()->int", PikaStdData_String_isalphaMethod);
    class_defineMethod(self, "isdigit()->int", PikaStdData_String_isdigitMethod);
    class_defineMethod(self, "islower()->int", PikaStdData_String_islowerMethod);
    class_defineMethod(self, "isspace()->int", PikaStdData_String_isspaceMethod);
    class_defineMethod(self, "replace(old:str,new:str)->str", PikaStdData_String_replaceMethod);
    class_defineMethod(self, "set(s:str)", PikaStdData_String_setMethod);
    class_defineMethod(self, "split(s:str)->List", PikaStdData_String_splitMethod);
    class_defineMethod(self, "startswith(prefix:str)->int", PikaStdData_String_startswithMethod);
    class_defineMethod(self, "strip()->str", PikaStdData_String_stripMethod);
    return self;
}

Arg *PikaStdData_String(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdData_String);
}
void PikaStdData_Tuple___del__Method(PikaObj *self, Args *args){
    PikaStdData_Tuple___del__(self);
}

void PikaStdData_Tuple___getitem__Method(PikaObj *self, Args *args){
    Arg* __key = args_getArg(args, "__key");
    Arg* res = PikaStdData_Tuple___getitem__(self, __key);
    method_returnArg(args, res);
}

void PikaStdData_Tuple___init__Method(PikaObj *self, Args *args){
    PikaStdData_Tuple___init__(self);
}

void PikaStdData_Tuple___iter__Method(PikaObj *self, Args *args){
    Arg* res = PikaStdData_Tuple___iter__(self);
    method_returnArg(args, res);
}

void PikaStdData_Tuple___len__Method(PikaObj *self, Args *args){
    int res = PikaStdData_Tuple___len__(self);
    method_returnInt(args, res);
}

void PikaStdData_Tuple___next__Method(PikaObj *self, Args *args){
    Arg* res = PikaStdData_Tuple___next__(self);
    method_returnArg(args, res);
}

void PikaStdData_Tuple___str__Method(PikaObj *self, Args *args){
    char* res = PikaStdData_Tuple___str__(self);
    method_returnStr(args, res);
}

void PikaStdData_Tuple_getMethod(PikaObj *self, Args *args){
    int i = args_getInt(args, "i");
    Arg* res = PikaStdData_Tuple_get(self, i);
    method_returnArg(args, res);
}

void PikaStdData_Tuple_lenMethod(PikaObj *self, Args *args){
    int res = PikaStdData_Tuple_len(self);
    method_returnInt(args, res);
}

PikaObj *New_PikaStdData_Tuple(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineMethod(self, "__del__()", PikaStdData_Tuple___del__Method);
    class_defineMethod(self, "__getitem__(__key:any)->any", PikaStdData_Tuple___getitem__Method);
    class_defineMethod(self, "__init__()", PikaStdData_Tuple___init__Method);
    class_defineMethod(self, "__iter__()->any", PikaStdData_Tuple___iter__Method);
    class_defineMethod(self, "__len__()->int", PikaStdData_Tuple___len__Method);
    class_defineMethod(self, "__next__()->any", PikaStdData_Tuple___next__Method);
    class_defineMethod(self, "__str__()->str", PikaStdData_Tuple___str__Method);
    class_defineMethod(self, "get(i:int)->any", PikaStdData_Tuple_getMethod);
    class_defineMethod(self, "len()->int", PikaStdData_Tuple_lenMethod);
    return self;
}

Arg *PikaStdData_Tuple(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdData_Tuple);
}
void PikaStdData_Utils_int_to_bytesMethod(PikaObj *self, Args *args){
    int val = args_getInt(args, "val");
    Arg* res = PikaStdData_Utils_int_to_bytes(self, val);
    method_returnArg(args, res);
}

PikaObj *New_PikaStdData_Utils(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineMethod(self, "int_to_bytes(val:int)->bytes", PikaStdData_Utils_int_to_bytesMethod);
    return self;
}

Arg *PikaStdData_Utils(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdData_Utils);
}
void PikaStdData_dict_keys___iter__Method(PikaObj *self, Args *args){
    Arg* res = PikaStdData_dict_keys___iter__(self);
    method_returnArg(args, res);
}

void PikaStdData_dict_keys___len__Method(PikaObj *self, Args *args){
    int res = PikaStdData_dict_keys___len__(self);
    method_returnInt(args, res);
}

void PikaStdData_dict_keys___next__Method(PikaObj *self, Args *args){
    Arg* res = PikaStdData_dict_keys___next__(self);
    method_returnArg(args, res);
}

void PikaStdData_dict_keys___str__Method(PikaObj *self, Args *args){
    char* res = PikaStdData_dict_keys___str__(self);
    method_returnStr(args, res);
}

PikaObj *New_PikaStdData_dict_keys(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineMethod(self, "__iter__()->any", PikaStdData_dict_keys___iter__Method);
    class_defineMethod(self, "__len__()->int", PikaStdData_dict_keys___len__Method);
    class_defineMethod(self, "__next__()->any", PikaStdData_dict_keys___next__Method);
    class_defineMethod(self, "__str__()->str", PikaStdData_dict_keys___str__Method);
    return self;
}

Arg *PikaStdData_dict_keys(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdData_dict_keys);
}
void PikaStdDevice_ADCMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdDevice_ADC(self);
    method_returnArg(args, res);
}

void PikaStdDevice_BaseDevMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdDevice_BaseDev(self);
    method_returnArg(args, res);
}

void PikaStdDevice_CANMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdDevice_CAN(self);
    method_returnArg(args, res);
}

void PikaStdDevice_GPIOMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdDevice_GPIO(self);
    method_returnArg(args, res);
}

void PikaStdDevice_IICMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdDevice_IIC(self);
    method_returnArg(args, res);
}

void PikaStdDevice_PWMMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdDevice_PWM(self);
    method_returnArg(args, res);
}

void PikaStdDevice_SPIMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdDevice_SPI(self);
    method_returnArg(args, res);
}

void PikaStdDevice_TimeMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdDevice_Time(self);
    method_returnArg(args, res);
}

void PikaStdDevice_UARTMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdDevice_UART(self);
    method_returnArg(args, res);
}

PikaObj *New_PikaStdDevice(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineConstructor(self, "ADC()->any", PikaStdDevice_ADCMethod);
    class_defineConstructor(self, "BaseDev()->any", PikaStdDevice_BaseDevMethod);
    class_defineConstructor(self, "CAN()->any", PikaStdDevice_CANMethod);
    class_defineConstructor(self, "GPIO()->any", PikaStdDevice_GPIOMethod);
    class_defineConstructor(self, "IIC()->any", PikaStdDevice_IICMethod);
    class_defineConstructor(self, "PWM()->any", PikaStdDevice_PWMMethod);
    class_defineConstructor(self, "SPI()->any", PikaStdDevice_SPIMethod);
    class_defineConstructor(self, "Time()->any", PikaStdDevice_TimeMethod);
    class_defineConstructor(self, "UART()->any", PikaStdDevice_UARTMethod);
    return self;
}

void PikaStdDevice_ADC___init__Method(PikaObj *self, Args *args){
    PikaStdDevice_ADC___init__(self);
}

void PikaStdDevice_ADC_disableMethod(PikaObj *self, Args *args){
    PikaStdDevice_ADC_disable(self);
}

void PikaStdDevice_ADC_enableMethod(PikaObj *self, Args *args){
    PikaStdDevice_ADC_enable(self);
}

void PikaStdDevice_ADC_platformDisableMethod(PikaObj *self, Args *args){
    PikaStdDevice_ADC_platformDisable(self);
}

void PikaStdDevice_ADC_platformEnableMethod(PikaObj *self, Args *args){
    PikaStdDevice_ADC_platformEnable(self);
}

void PikaStdDevice_ADC_platformReadMethod(PikaObj *self, Args *args){
    PikaStdDevice_ADC_platformRead(self);
}

void PikaStdDevice_ADC_readMethod(PikaObj *self, Args *args){
    double res = PikaStdDevice_ADC_read(self);
    method_returnFloat(args, res);
}

void PikaStdDevice_ADC_setPinMethod(PikaObj *self, Args *args){
    char* pin = args_getStr(args, "pin");
    PikaStdDevice_ADC_setPin(self, pin);
}

PikaObj *New_PikaStdDevice_ADC(Args *args){
    PikaObj *self = New_PikaStdDevice_BaseDev(args);
    class_defineMethod(self, "__init__()", PikaStdDevice_ADC___init__Method);
    class_defineMethod(self, "disable()", PikaStdDevice_ADC_disableMethod);
    class_defineMethod(self, "enable()", PikaStdDevice_ADC_enableMethod);
    class_defineMethod(self, "platformDisable()", PikaStdDevice_ADC_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", PikaStdDevice_ADC_platformEnableMethod);
    class_defineMethod(self, "platformRead()", PikaStdDevice_ADC_platformReadMethod);
    class_defineMethod(self, "read()->float", PikaStdDevice_ADC_readMethod);
    class_defineMethod(self, "setPin(pin:str)", PikaStdDevice_ADC_setPinMethod);
    return self;
}

Arg *PikaStdDevice_ADC(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdDevice_ADC);
}
void PikaStdDevice_BaseDev_addEventCallBackMethod(PikaObj *self, Args *args){
    Arg* eventCallback = args_getArg(args, "eventCallback");
    PikaStdDevice_BaseDev_addEventCallBack(self, eventCallback);
}

void PikaStdDevice_BaseDev_platformGetEventIdMethod(PikaObj *self, Args *args){
    PikaStdDevice_BaseDev_platformGetEventId(self);
}

PikaObj *New_PikaStdDevice_BaseDev(Args *args){
    PikaObj *self = New_TinyObj(args);
#if PIKA_EVENT_ENABLE
    class_defineMethod(self, "addEventCallBack(eventCallback:any)", PikaStdDevice_BaseDev_addEventCallBackMethod);
#endif
#if PIKA_EVENT_ENABLE
    class_defineMethod(self, "platformGetEventId()", PikaStdDevice_BaseDev_platformGetEventIdMethod);
#endif
    return self;
}

Arg *PikaStdDevice_BaseDev(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdDevice_BaseDev);
}
void PikaStdDevice_CAN___init__Method(PikaObj *self, Args *args){
    PikaStdDevice_CAN___init__(self);
}

void PikaStdDevice_CAN_addFilterMethod(PikaObj *self, Args *args){
    int id = args_getInt(args, "id");
    int ide = args_getInt(args, "ide");
    int rtr = args_getInt(args, "rtr");
    int mode = args_getInt(args, "mode");
    int mask = args_getInt(args, "mask");
    int hdr = args_getInt(args, "hdr");
    PikaStdDevice_CAN_addFilter(self, id, ide, rtr, mode, mask, hdr);
}

void PikaStdDevice_CAN_disableMethod(PikaObj *self, Args *args){
    PikaStdDevice_CAN_disable(self);
}

void PikaStdDevice_CAN_enableMethod(PikaObj *self, Args *args){
    PikaStdDevice_CAN_enable(self);
}

void PikaStdDevice_CAN_platformDisableMethod(PikaObj *self, Args *args){
    PikaStdDevice_CAN_platformDisable(self);
}

void PikaStdDevice_CAN_platformEnableMethod(PikaObj *self, Args *args){
    PikaStdDevice_CAN_platformEnable(self);
}

void PikaStdDevice_CAN_platformReadMethod(PikaObj *self, Args *args){
    PikaStdDevice_CAN_platformRead(self);
}

void PikaStdDevice_CAN_platformReadBytesMethod(PikaObj *self, Args *args){
    PikaStdDevice_CAN_platformReadBytes(self);
}

void PikaStdDevice_CAN_platformWriteMethod(PikaObj *self, Args *args){
    PikaStdDevice_CAN_platformWrite(self);
}

void PikaStdDevice_CAN_platformWriteBytesMethod(PikaObj *self, Args *args){
    PikaStdDevice_CAN_platformWriteBytes(self);
}

void PikaStdDevice_CAN_readMethod(PikaObj *self, Args *args){
    int length = args_getInt(args, "length");
    char* res = PikaStdDevice_CAN_read(self, length);
    method_returnStr(args, res);
}

void PikaStdDevice_CAN_readBytesMethod(PikaObj *self, Args *args){
    int length = args_getInt(args, "length");
    Arg* res = PikaStdDevice_CAN_readBytes(self, length);
    method_returnArg(args, res);
}

void PikaStdDevice_CAN_setBaudRateMethod(PikaObj *self, Args *args){
    int baudRate = args_getInt(args, "baudRate");
    PikaStdDevice_CAN_setBaudRate(self, baudRate);
}

void PikaStdDevice_CAN_setIdMethod(PikaObj *self, Args *args){
    int id = args_getInt(args, "id");
    PikaStdDevice_CAN_setId(self, id);
}

void PikaStdDevice_CAN_setModeMethod(PikaObj *self, Args *args){
    char* mode = args_getStr(args, "mode");
    PikaStdDevice_CAN_setMode(self, mode);
}

void PikaStdDevice_CAN_setNameMethod(PikaObj *self, Args *args){
    char* name = args_getStr(args, "name");
    PikaStdDevice_CAN_setName(self, name);
}

void PikaStdDevice_CAN_writeMethod(PikaObj *self, Args *args){
    char* data = args_getStr(args, "data");
    PikaStdDevice_CAN_write(self, data);
}

void PikaStdDevice_CAN_writeBytesMethod(PikaObj *self, Args *args){
    uint8_t* data = args_getBytes(args, "data");
    int length = args_getInt(args, "length");
    PikaStdDevice_CAN_writeBytes(self, data, length);
}

PikaObj *New_PikaStdDevice_CAN(Args *args){
    PikaObj *self = New_PikaStdDevice_BaseDev(args);
    class_defineMethod(self, "__init__()", PikaStdDevice_CAN___init__Method);
    class_defineMethod(self, "addFilter(id:int,ide:int,rtr:int,mode:int,mask:int,hdr:int)", PikaStdDevice_CAN_addFilterMethod);
    class_defineMethod(self, "disable()", PikaStdDevice_CAN_disableMethod);
    class_defineMethod(self, "enable()", PikaStdDevice_CAN_enableMethod);
    class_defineMethod(self, "platformDisable()", PikaStdDevice_CAN_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", PikaStdDevice_CAN_platformEnableMethod);
    class_defineMethod(self, "platformRead()", PikaStdDevice_CAN_platformReadMethod);
    class_defineMethod(self, "platformReadBytes()", PikaStdDevice_CAN_platformReadBytesMethod);
    class_defineMethod(self, "platformWrite()", PikaStdDevice_CAN_platformWriteMethod);
    class_defineMethod(self, "platformWriteBytes()", PikaStdDevice_CAN_platformWriteBytesMethod);
    class_defineMethod(self, "read(length:int)->str", PikaStdDevice_CAN_readMethod);
    class_defineMethod(self, "readBytes(length:int)->bytes", PikaStdDevice_CAN_readBytesMethod);
    class_defineMethod(self, "setBaudRate(baudRate:int)", PikaStdDevice_CAN_setBaudRateMethod);
    class_defineMethod(self, "setId(id:int)", PikaStdDevice_CAN_setIdMethod);
    class_defineMethod(self, "setMode(mode:str)", PikaStdDevice_CAN_setModeMethod);
    class_defineMethod(self, "setName(name:str)", PikaStdDevice_CAN_setNameMethod);
    class_defineMethod(self, "write(data:str)", PikaStdDevice_CAN_writeMethod);
    class_defineMethod(self, "writeBytes(data:bytes,length:int)", PikaStdDevice_CAN_writeBytesMethod);
    return self;
}

Arg *PikaStdDevice_CAN(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdDevice_CAN);
}
void PikaStdDevice_GPIO___init__Method(PikaObj *self, Args *args){
    PikaStdDevice_GPIO___init__(self);
}

void PikaStdDevice_GPIO_disableMethod(PikaObj *self, Args *args){
    PikaStdDevice_GPIO_disable(self);
}

void PikaStdDevice_GPIO_enableMethod(PikaObj *self, Args *args){
    PikaStdDevice_GPIO_enable(self);
}

void PikaStdDevice_GPIO_getIdMethod(PikaObj *self, Args *args){
    int res = PikaStdDevice_GPIO_getId(self);
    method_returnInt(args, res);
}

void PikaStdDevice_GPIO_getModeMethod(PikaObj *self, Args *args){
    char* res = PikaStdDevice_GPIO_getMode(self);
    method_returnStr(args, res);
}

void PikaStdDevice_GPIO_getPinMethod(PikaObj *self, Args *args){
    char* res = PikaStdDevice_GPIO_getPin(self);
    method_returnStr(args, res);
}

void PikaStdDevice_GPIO_highMethod(PikaObj *self, Args *args){
    PikaStdDevice_GPIO_high(self);
}

void PikaStdDevice_GPIO_lowMethod(PikaObj *self, Args *args){
    PikaStdDevice_GPIO_low(self);
}

void PikaStdDevice_GPIO_platformDisableMethod(PikaObj *self, Args *args){
    PikaStdDevice_GPIO_platformDisable(self);
}

void PikaStdDevice_GPIO_platformEnableMethod(PikaObj *self, Args *args){
    PikaStdDevice_GPIO_platformEnable(self);
}

void PikaStdDevice_GPIO_platformHighMethod(PikaObj *self, Args *args){
    PikaStdDevice_GPIO_platformHigh(self);
}

void PikaStdDevice_GPIO_platformLowMethod(PikaObj *self, Args *args){
    PikaStdDevice_GPIO_platformLow(self);
}

void PikaStdDevice_GPIO_platformReadMethod(PikaObj *self, Args *args){
    PikaStdDevice_GPIO_platformRead(self);
}

void PikaStdDevice_GPIO_platformSetModeMethod(PikaObj *self, Args *args){
    PikaStdDevice_GPIO_platformSetMode(self);
}

void PikaStdDevice_GPIO_readMethod(PikaObj *self, Args *args){
    int res = PikaStdDevice_GPIO_read(self);
    method_returnInt(args, res);
}

void PikaStdDevice_GPIO_setIdMethod(PikaObj *self, Args *args){
    int id = args_getInt(args, "id");
    PikaStdDevice_GPIO_setId(self, id);
}

void PikaStdDevice_GPIO_setModeMethod(PikaObj *self, Args *args){
    char* mode = args_getStr(args, "mode");
    PikaStdDevice_GPIO_setMode(self, mode);
}

void PikaStdDevice_GPIO_setPinMethod(PikaObj *self, Args *args){
    char* pinName = args_getStr(args, "pinName");
    PikaStdDevice_GPIO_setPin(self, pinName);
}

void PikaStdDevice_GPIO_setPullMethod(PikaObj *self, Args *args){
    char* pull = args_getStr(args, "pull");
    PikaStdDevice_GPIO_setPull(self, pull);
}

PikaObj *New_PikaStdDevice_GPIO(Args *args){
    PikaObj *self = New_PikaStdDevice_BaseDev(args);
    class_defineMethod(self, "__init__()", PikaStdDevice_GPIO___init__Method);
    class_defineMethod(self, "disable()", PikaStdDevice_GPIO_disableMethod);
    class_defineMethod(self, "enable()", PikaStdDevice_GPIO_enableMethod);
    class_defineMethod(self, "getId()->int", PikaStdDevice_GPIO_getIdMethod);
    class_defineMethod(self, "getMode()->str", PikaStdDevice_GPIO_getModeMethod);
    class_defineMethod(self, "getPin()->str", PikaStdDevice_GPIO_getPinMethod);
    class_defineMethod(self, "high()", PikaStdDevice_GPIO_highMethod);
    class_defineMethod(self, "low()", PikaStdDevice_GPIO_lowMethod);
    class_defineMethod(self, "platformDisable()", PikaStdDevice_GPIO_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", PikaStdDevice_GPIO_platformEnableMethod);
    class_defineMethod(self, "platformHigh()", PikaStdDevice_GPIO_platformHighMethod);
    class_defineMethod(self, "platformLow()", PikaStdDevice_GPIO_platformLowMethod);
    class_defineMethod(self, "platformRead()", PikaStdDevice_GPIO_platformReadMethod);
    class_defineMethod(self, "platformSetMode()", PikaStdDevice_GPIO_platformSetModeMethod);
    class_defineMethod(self, "read()->int", PikaStdDevice_GPIO_readMethod);
    class_defineMethod(self, "setId(id:int)", PikaStdDevice_GPIO_setIdMethod);
    class_defineMethod(self, "setMode(mode:str)", PikaStdDevice_GPIO_setModeMethod);
    class_defineMethod(self, "setPin(pinName:str)", PikaStdDevice_GPIO_setPinMethod);
    class_defineMethod(self, "setPull(pull:str)", PikaStdDevice_GPIO_setPullMethod);
    return self;
}

Arg *PikaStdDevice_GPIO(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdDevice_GPIO);
}
void PikaStdDevice_IIC___init__Method(PikaObj *self, Args *args){
    PikaStdDevice_IIC___init__(self);
}

void PikaStdDevice_IIC_disableMethod(PikaObj *self, Args *args){
    PikaStdDevice_IIC_disable(self);
}

void PikaStdDevice_IIC_enableMethod(PikaObj *self, Args *args){
    PikaStdDevice_IIC_enable(self);
}

void PikaStdDevice_IIC_platformDisableMethod(PikaObj *self, Args *args){
    PikaStdDevice_IIC_platformDisable(self);
}

void PikaStdDevice_IIC_platformEnableMethod(PikaObj *self, Args *args){
    PikaStdDevice_IIC_platformEnable(self);
}

void PikaStdDevice_IIC_platformReadMethod(PikaObj *self, Args *args){
    PikaStdDevice_IIC_platformRead(self);
}

void PikaStdDevice_IIC_platformReadBytesMethod(PikaObj *self, Args *args){
    PikaStdDevice_IIC_platformReadBytes(self);
}

void PikaStdDevice_IIC_platformWriteMethod(PikaObj *self, Args *args){
    PikaStdDevice_IIC_platformWrite(self);
}

void PikaStdDevice_IIC_platformWriteBytesMethod(PikaObj *self, Args *args){
    PikaStdDevice_IIC_platformWriteBytes(self);
}

void PikaStdDevice_IIC_readMethod(PikaObj *self, Args *args){
    int addr = args_getInt(args, "addr");
    int length = args_getInt(args, "length");
    char* res = PikaStdDevice_IIC_read(self, addr, length);
    method_returnStr(args, res);
}

void PikaStdDevice_IIC_readBytesMethod(PikaObj *self, Args *args){
    int addr = args_getInt(args, "addr");
    int length = args_getInt(args, "length");
    Arg* res = PikaStdDevice_IIC_readBytes(self, addr, length);
    method_returnArg(args, res);
}

void PikaStdDevice_IIC_setDeviceAddrMethod(PikaObj *self, Args *args){
    int addr = args_getInt(args, "addr");
    PikaStdDevice_IIC_setDeviceAddr(self, addr);
}

void PikaStdDevice_IIC_setPinSCLMethod(PikaObj *self, Args *args){
    char* pin = args_getStr(args, "pin");
    PikaStdDevice_IIC_setPinSCL(self, pin);
}

void PikaStdDevice_IIC_setPinSDAMethod(PikaObj *self, Args *args){
    char* pin = args_getStr(args, "pin");
    PikaStdDevice_IIC_setPinSDA(self, pin);
}

void PikaStdDevice_IIC_writeMethod(PikaObj *self, Args *args){
    int addr = args_getInt(args, "addr");
    char* data = args_getStr(args, "data");
    PikaStdDevice_IIC_write(self, addr, data);
}

void PikaStdDevice_IIC_writeBytesMethod(PikaObj *self, Args *args){
    int addr = args_getInt(args, "addr");
    uint8_t* data = args_getBytes(args, "data");
    int length = args_getInt(args, "length");
    PikaStdDevice_IIC_writeBytes(self, addr, data, length);
}

PikaObj *New_PikaStdDevice_IIC(Args *args){
    PikaObj *self = New_PikaStdDevice_BaseDev(args);
    class_defineMethod(self, "__init__()", PikaStdDevice_IIC___init__Method);
    class_defineMethod(self, "disable()", PikaStdDevice_IIC_disableMethod);
    class_defineMethod(self, "enable()", PikaStdDevice_IIC_enableMethod);
    class_defineMethod(self, "platformDisable()", PikaStdDevice_IIC_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", PikaStdDevice_IIC_platformEnableMethod);
    class_defineMethod(self, "platformRead()", PikaStdDevice_IIC_platformReadMethod);
    class_defineMethod(self, "platformReadBytes()", PikaStdDevice_IIC_platformReadBytesMethod);
    class_defineMethod(self, "platformWrite()", PikaStdDevice_IIC_platformWriteMethod);
    class_defineMethod(self, "platformWriteBytes()", PikaStdDevice_IIC_platformWriteBytesMethod);
    class_defineMethod(self, "read(addr:int,length:int)->str", PikaStdDevice_IIC_readMethod);
    class_defineMethod(self, "readBytes(addr:int,length:int)->bytes", PikaStdDevice_IIC_readBytesMethod);
    class_defineMethod(self, "setDeviceAddr(addr:int)", PikaStdDevice_IIC_setDeviceAddrMethod);
    class_defineMethod(self, "setPinSCL(pin:str)", PikaStdDevice_IIC_setPinSCLMethod);
    class_defineMethod(self, "setPinSDA(pin:str)", PikaStdDevice_IIC_setPinSDAMethod);
    class_defineMethod(self, "write(addr:int,data:str)", PikaStdDevice_IIC_writeMethod);
    class_defineMethod(self, "writeBytes(addr:int,data:bytes,length:int)", PikaStdDevice_IIC_writeBytesMethod);
    return self;
}

Arg *PikaStdDevice_IIC(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdDevice_IIC);
}
void PikaStdDevice_PWM___init__Method(PikaObj *self, Args *args){
    PikaStdDevice_PWM___init__(self);
}

void PikaStdDevice_PWM_disableMethod(PikaObj *self, Args *args){
    PikaStdDevice_PWM_disable(self);
}

void PikaStdDevice_PWM_enableMethod(PikaObj *self, Args *args){
    PikaStdDevice_PWM_enable(self);
}

void PikaStdDevice_PWM_getChannelMethod(PikaObj *self, Args *args){
    int res = PikaStdDevice_PWM_getChannel(self);
    method_returnInt(args, res);
}

void PikaStdDevice_PWM_getDutyMethod(PikaObj *self, Args *args){
    double res = PikaStdDevice_PWM_getDuty(self);
    method_returnFloat(args, res);
}

void PikaStdDevice_PWM_getFrequencyMethod(PikaObj *self, Args *args){
    int res = PikaStdDevice_PWM_getFrequency(self);
    method_returnInt(args, res);
}

void PikaStdDevice_PWM_getNameMethod(PikaObj *self, Args *args){
    char* res = PikaStdDevice_PWM_getName(self);
    method_returnStr(args, res);
}

void PikaStdDevice_PWM_platformDisableMethod(PikaObj *self, Args *args){
    PikaStdDevice_PWM_platformDisable(self);
}

void PikaStdDevice_PWM_platformEnableMethod(PikaObj *self, Args *args){
    PikaStdDevice_PWM_platformEnable(self);
}

void PikaStdDevice_PWM_platformSetDutyMethod(PikaObj *self, Args *args){
    PikaStdDevice_PWM_platformSetDuty(self);
}

void PikaStdDevice_PWM_platformSetFrequencyMethod(PikaObj *self, Args *args){
    PikaStdDevice_PWM_platformSetFrequency(self);
}

void PikaStdDevice_PWM_setChannelMethod(PikaObj *self, Args *args){
    int ch = args_getInt(args, "ch");
    PikaStdDevice_PWM_setChannel(self, ch);
}

void PikaStdDevice_PWM_setDutyMethod(PikaObj *self, Args *args){
    double duty = args_getFloat(args, "duty");
    PikaStdDevice_PWM_setDuty(self, duty);
}

void PikaStdDevice_PWM_setFreqMethod(PikaObj *self, Args *args){
    int freq = args_getInt(args, "freq");
    PikaStdDevice_PWM_setFreq(self, freq);
}

void PikaStdDevice_PWM_setFrequencyMethod(PikaObj *self, Args *args){
    int freq = args_getInt(args, "freq");
    PikaStdDevice_PWM_setFrequency(self, freq);
}

void PikaStdDevice_PWM_setNameMethod(PikaObj *self, Args *args){
    char* name = args_getStr(args, "name");
    PikaStdDevice_PWM_setName(self, name);
}

void PikaStdDevice_PWM_setPinMethod(PikaObj *self, Args *args){
    char* pin = args_getStr(args, "pin");
    PikaStdDevice_PWM_setPin(self, pin);
}

PikaObj *New_PikaStdDevice_PWM(Args *args){
    PikaObj *self = New_PikaStdDevice_BaseDev(args);
    class_defineMethod(self, "__init__()", PikaStdDevice_PWM___init__Method);
    class_defineMethod(self, "disable()", PikaStdDevice_PWM_disableMethod);
    class_defineMethod(self, "enable()", PikaStdDevice_PWM_enableMethod);
    class_defineMethod(self, "getChannel()->int", PikaStdDevice_PWM_getChannelMethod);
    class_defineMethod(self, "getDuty()->float", PikaStdDevice_PWM_getDutyMethod);
    class_defineMethod(self, "getFrequency()->int", PikaStdDevice_PWM_getFrequencyMethod);
    class_defineMethod(self, "getName()->str", PikaStdDevice_PWM_getNameMethod);
    class_defineMethod(self, "platformDisable()", PikaStdDevice_PWM_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", PikaStdDevice_PWM_platformEnableMethod);
    class_defineMethod(self, "platformSetDuty()", PikaStdDevice_PWM_platformSetDutyMethod);
    class_defineMethod(self, "platformSetFrequency()", PikaStdDevice_PWM_platformSetFrequencyMethod);
    class_defineMethod(self, "setChannel(ch:int)", PikaStdDevice_PWM_setChannelMethod);
    class_defineMethod(self, "setDuty(duty:float)", PikaStdDevice_PWM_setDutyMethod);
    class_defineMethod(self, "setFreq(freq:int)", PikaStdDevice_PWM_setFreqMethod);
    class_defineMethod(self, "setFrequency(freq:int)", PikaStdDevice_PWM_setFrequencyMethod);
    class_defineMethod(self, "setName(name:str)", PikaStdDevice_PWM_setNameMethod);
    class_defineMethod(self, "setPin(pin:str)", PikaStdDevice_PWM_setPinMethod);
    return self;
}

Arg *PikaStdDevice_PWM(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdDevice_PWM);
}
void PikaStdDevice_SPI___init__Method(PikaObj *self, Args *args){
    PikaStdDevice_SPI___init__(self);
}

void PikaStdDevice_SPI_disableMethod(PikaObj *self, Args *args){
    PikaStdDevice_SPI_disable(self);
}

void PikaStdDevice_SPI_enableMethod(PikaObj *self, Args *args){
    PikaStdDevice_SPI_enable(self);
}

void PikaStdDevice_SPI_platformDisableMethod(PikaObj *self, Args *args){
    PikaStdDevice_SPI_platformDisable(self);
}

void PikaStdDevice_SPI_platformEnableMethod(PikaObj *self, Args *args){
    PikaStdDevice_SPI_platformEnable(self);
}

void PikaStdDevice_SPI_platformReadMethod(PikaObj *self, Args *args){
    PikaStdDevice_SPI_platformRead(self);
}

void PikaStdDevice_SPI_platformReadBytesMethod(PikaObj *self, Args *args){
    PikaStdDevice_SPI_platformReadBytes(self);
}

void PikaStdDevice_SPI_platformWriteMethod(PikaObj *self, Args *args){
    PikaStdDevice_SPI_platformWrite(self);
}

void PikaStdDevice_SPI_platformWriteBytesMethod(PikaObj *self, Args *args){
    PikaStdDevice_SPI_platformWriteBytes(self);
}

void PikaStdDevice_SPI_readMethod(PikaObj *self, Args *args){
    int length = args_getInt(args, "length");
    char* res = PikaStdDevice_SPI_read(self, length);
    method_returnStr(args, res);
}

void PikaStdDevice_SPI_readBytesMethod(PikaObj *self, Args *args){
    int length = args_getInt(args, "length");
    Arg* res = PikaStdDevice_SPI_readBytes(self, length);
    method_returnArg(args, res);
}

void PikaStdDevice_SPI_setBaudRateMethod(PikaObj *self, Args *args){
    int baudRate = args_getInt(args, "baudRate");
    PikaStdDevice_SPI_setBaudRate(self, baudRate);
}

void PikaStdDevice_SPI_setIdMethod(PikaObj *self, Args *args){
    int id = args_getInt(args, "id");
    PikaStdDevice_SPI_setId(self, id);
}

void PikaStdDevice_SPI_setNameMethod(PikaObj *self, Args *args){
    char* name = args_getStr(args, "name");
    PikaStdDevice_SPI_setName(self, name);
}

void PikaStdDevice_SPI_setPhaseMethod(PikaObj *self, Args *args){
    int phase = args_getInt(args, "phase");
    PikaStdDevice_SPI_setPhase(self, phase);
}

void PikaStdDevice_SPI_setPinMISOMethod(PikaObj *self, Args *args){
    char* pin = args_getStr(args, "pin");
    PikaStdDevice_SPI_setPinMISO(self, pin);
}

void PikaStdDevice_SPI_setPinMOSIMethod(PikaObj *self, Args *args){
    char* pin = args_getStr(args, "pin");
    PikaStdDevice_SPI_setPinMOSI(self, pin);
}

void PikaStdDevice_SPI_setPinSCKMethod(PikaObj *self, Args *args){
    char* pin = args_getStr(args, "pin");
    PikaStdDevice_SPI_setPinSCK(self, pin);
}

void PikaStdDevice_SPI_setPolarityMethod(PikaObj *self, Args *args){
    int polarity = args_getInt(args, "polarity");
    PikaStdDevice_SPI_setPolarity(self, polarity);
}

void PikaStdDevice_SPI_writeMethod(PikaObj *self, Args *args){
    char* data = args_getStr(args, "data");
    PikaStdDevice_SPI_write(self, data);
}

void PikaStdDevice_SPI_writeBytesMethod(PikaObj *self, Args *args){
    uint8_t* data = args_getBytes(args, "data");
    int length = args_getInt(args, "length");
    PikaStdDevice_SPI_writeBytes(self, data, length);
}

PikaObj *New_PikaStdDevice_SPI(Args *args){
    PikaObj *self = New_PikaStdDevice_BaseDev(args);
    class_defineMethod(self, "__init__()", PikaStdDevice_SPI___init__Method);
    class_defineMethod(self, "disable()", PikaStdDevice_SPI_disableMethod);
    class_defineMethod(self, "enable()", PikaStdDevice_SPI_enableMethod);
    class_defineMethod(self, "platformDisable()", PikaStdDevice_SPI_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", PikaStdDevice_SPI_platformEnableMethod);
    class_defineMethod(self, "platformRead()", PikaStdDevice_SPI_platformReadMethod);
    class_defineMethod(self, "platformReadBytes()", PikaStdDevice_SPI_platformReadBytesMethod);
    class_defineMethod(self, "platformWrite()", PikaStdDevice_SPI_platformWriteMethod);
    class_defineMethod(self, "platformWriteBytes()", PikaStdDevice_SPI_platformWriteBytesMethod);
    class_defineMethod(self, "read(length:int)->str", PikaStdDevice_SPI_readMethod);
    class_defineMethod(self, "readBytes(length:int)->bytes", PikaStdDevice_SPI_readBytesMethod);
    class_defineMethod(self, "setBaudRate(baudRate:int)", PikaStdDevice_SPI_setBaudRateMethod);
    class_defineMethod(self, "setId(id:int)", PikaStdDevice_SPI_setIdMethod);
    class_defineMethod(self, "setName(name:str)", PikaStdDevice_SPI_setNameMethod);
    class_defineMethod(self, "setPhase(phase:int)", PikaStdDevice_SPI_setPhaseMethod);
    class_defineMethod(self, "setPinMISO(pin:str)", PikaStdDevice_SPI_setPinMISOMethod);
    class_defineMethod(self, "setPinMOSI(pin:str)", PikaStdDevice_SPI_setPinMOSIMethod);
    class_defineMethod(self, "setPinSCK(pin:str)", PikaStdDevice_SPI_setPinSCKMethod);
    class_defineMethod(self, "setPolarity(polarity:int)", PikaStdDevice_SPI_setPolarityMethod);
    class_defineMethod(self, "write(data:str)", PikaStdDevice_SPI_writeMethod);
    class_defineMethod(self, "writeBytes(data:bytes,length:int)", PikaStdDevice_SPI_writeBytesMethod);
    return self;
}

Arg *PikaStdDevice_SPI(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdDevice_SPI);
}
void PikaStdDevice_Time___init__Method(PikaObj *self, Args *args){
    PikaStdDevice_Time___init__(self);
}

void PikaStdDevice_Time_asctimeMethod(PikaObj *self, Args *args){
    PikaStdDevice_Time_asctime(self);
}

void PikaStdDevice_Time_ctimeMethod(PikaObj *self, Args *args){
    double unix_time = args_getFloat(args, "unix_time");
    PikaStdDevice_Time_ctime(self, unix_time);
}

void PikaStdDevice_Time_gmtimeMethod(PikaObj *self, Args *args){
    double unix_time = args_getFloat(args, "unix_time");
    PikaStdDevice_Time_gmtime(self, unix_time);
}

void PikaStdDevice_Time_localtimeMethod(PikaObj *self, Args *args){
    double unix_time = args_getFloat(args, "unix_time");
    PikaStdDevice_Time_localtime(self, unix_time);
}

void PikaStdDevice_Time_mktimeMethod(PikaObj *self, Args *args){
    int res = PikaStdDevice_Time_mktime(self);
    method_returnInt(args, res);
}

void PikaStdDevice_Time_platformGetTickMethod(PikaObj *self, Args *args){
    PikaStdDevice_Time_platformGetTick(self);
}

void PikaStdDevice_Time_sleepMethod(PikaObj *self, Args *args){
    double s = args_getFloat(args, "s");
    PikaStdDevice_Time_sleep(self, s);
}

void PikaStdDevice_Time_sleep_msMethod(PikaObj *self, Args *args){
    int ms = args_getInt(args, "ms");
    PikaStdDevice_Time_sleep_ms(self, ms);
}

void PikaStdDevice_Time_sleep_sMethod(PikaObj *self, Args *args){
    int s = args_getInt(args, "s");
    PikaStdDevice_Time_sleep_s(self, s);
}

void PikaStdDevice_Time_timeMethod(PikaObj *self, Args *args){
    double res = PikaStdDevice_Time_time(self);
    method_returnFloat(args, res);
}

void PikaStdDevice_Time_time_nsMethod(PikaObj *self, Args *args){
    int res = PikaStdDevice_Time_time_ns(self);
    method_returnInt(args, res);
}

PikaObj *New_PikaStdDevice_Time(Args *args){
    PikaObj *self = New_PikaStdDevice_BaseDev(args);
    class_defineMethod(self, "__init__()", PikaStdDevice_Time___init__Method);
#if PIKA_STD_DEVICE_UNIX_TIME_ENABLE
    class_defineMethod(self, "asctime()", PikaStdDevice_Time_asctimeMethod);
#endif
#if PIKA_STD_DEVICE_UNIX_TIME_ENABLE
    class_defineMethod(self, "ctime(unix_time:float)", PikaStdDevice_Time_ctimeMethod);
#endif
#if PIKA_STD_DEVICE_UNIX_TIME_ENABLE
    class_defineMethod(self, "gmtime(unix_time:float)", PikaStdDevice_Time_gmtimeMethod);
#endif
#if PIKA_STD_DEVICE_UNIX_TIME_ENABLE
    class_defineMethod(self, "localtime(unix_time:float)", PikaStdDevice_Time_localtimeMethod);
#endif
#if PIKA_STD_DEVICE_UNIX_TIME_ENABLE
    class_defineMethod(self, "mktime()->int", PikaStdDevice_Time_mktimeMethod);
#endif
#if PIKA_STD_DEVICE_UNIX_TIME_ENABLE
    class_defineMethod(self, "platformGetTick()", PikaStdDevice_Time_platformGetTickMethod);
#endif
    class_defineMethod(self, "sleep(s:float)", PikaStdDevice_Time_sleepMethod);
    class_defineMethod(self, "sleep_ms(ms:int)", PikaStdDevice_Time_sleep_msMethod);
    class_defineMethod(self, "sleep_s(s:int)", PikaStdDevice_Time_sleep_sMethod);
#if PIKA_STD_DEVICE_UNIX_TIME_ENABLE
    class_defineMethod(self, "time()->float", PikaStdDevice_Time_timeMethod);
#endif
#if PIKA_STD_DEVICE_UNIX_TIME_ENABLE
    class_defineMethod(self, "time_ns()->int", PikaStdDevice_Time_time_nsMethod);
#endif
    return self;
}

Arg *PikaStdDevice_Time(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdDevice_Time);
}
void PikaStdDevice_UART___init__Method(PikaObj *self, Args *args){
    PikaStdDevice_UART___init__(self);
}

void PikaStdDevice_UART_disableMethod(PikaObj *self, Args *args){
    PikaStdDevice_UART_disable(self);
}

void PikaStdDevice_UART_enableMethod(PikaObj *self, Args *args){
    PikaStdDevice_UART_enable(self);
}

void PikaStdDevice_UART_platformDisableMethod(PikaObj *self, Args *args){
    PikaStdDevice_UART_platformDisable(self);
}

void PikaStdDevice_UART_platformEnableMethod(PikaObj *self, Args *args){
    PikaStdDevice_UART_platformEnable(self);
}

void PikaStdDevice_UART_platformReadMethod(PikaObj *self, Args *args){
    PikaStdDevice_UART_platformRead(self);
}

void PikaStdDevice_UART_platformReadBytesMethod(PikaObj *self, Args *args){
    PikaStdDevice_UART_platformReadBytes(self);
}

void PikaStdDevice_UART_platformWriteMethod(PikaObj *self, Args *args){
    PikaStdDevice_UART_platformWrite(self);
}

void PikaStdDevice_UART_platformWriteBytesMethod(PikaObj *self, Args *args){
    PikaStdDevice_UART_platformWriteBytes(self);
}

void PikaStdDevice_UART_readMethod(PikaObj *self, Args *args){
    int length = args_getInt(args, "length");
    char* res = PikaStdDevice_UART_read(self, length);
    method_returnStr(args, res);
}

void PikaStdDevice_UART_readBytesMethod(PikaObj *self, Args *args){
    int length = args_getInt(args, "length");
    Arg* res = PikaStdDevice_UART_readBytes(self, length);
    method_returnArg(args, res);
}

void PikaStdDevice_UART_setBaudRateMethod(PikaObj *self, Args *args){
    int baudRate = args_getInt(args, "baudRate");
    PikaStdDevice_UART_setBaudRate(self, baudRate);
}

void PikaStdDevice_UART_setIdMethod(PikaObj *self, Args *args){
    int id = args_getInt(args, "id");
    PikaStdDevice_UART_setId(self, id);
}

void PikaStdDevice_UART_writeMethod(PikaObj *self, Args *args){
    char* data = args_getStr(args, "data");
    PikaStdDevice_UART_write(self, data);
}

void PikaStdDevice_UART_writeBytesMethod(PikaObj *self, Args *args){
    uint8_t* data = args_getBytes(args, "data");
    int length = args_getInt(args, "length");
    PikaStdDevice_UART_writeBytes(self, data, length);
}

PikaObj *New_PikaStdDevice_UART(Args *args){
    PikaObj *self = New_PikaStdDevice_BaseDev(args);
    class_defineMethod(self, "__init__()", PikaStdDevice_UART___init__Method);
    class_defineMethod(self, "disable()", PikaStdDevice_UART_disableMethod);
    class_defineMethod(self, "enable()", PikaStdDevice_UART_enableMethod);
    class_defineMethod(self, "platformDisable()", PikaStdDevice_UART_platformDisableMethod);
    class_defineMethod(self, "platformEnable()", PikaStdDevice_UART_platformEnableMethod);
    class_defineMethod(self, "platformRead()", PikaStdDevice_UART_platformReadMethod);
    class_defineMethod(self, "platformReadBytes()", PikaStdDevice_UART_platformReadBytesMethod);
    class_defineMethod(self, "platformWrite()", PikaStdDevice_UART_platformWriteMethod);
    class_defineMethod(self, "platformWriteBytes()", PikaStdDevice_UART_platformWriteBytesMethod);
    class_defineMethod(self, "read(length:int)->str", PikaStdDevice_UART_readMethod);
    class_defineMethod(self, "readBytes(length:int)->bytes", PikaStdDevice_UART_readBytesMethod);
    class_defineMethod(self, "setBaudRate(baudRate:int)", PikaStdDevice_UART_setBaudRateMethod);
    class_defineMethod(self, "setId(id:int)", PikaStdDevice_UART_setIdMethod);
    class_defineMethod(self, "write(data:str)", PikaStdDevice_UART_writeMethod);
    class_defineMethod(self, "writeBytes(data:bytes,length:int)", PikaStdDevice_UART_writeBytesMethod);
    return self;
}

Arg *PikaStdDevice_UART(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdDevice_UART);
}
void PikaStdLib_MemCheckerMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdLib_MemChecker(self);
    method_returnArg(args, res);
}

void PikaStdLib_RangeObjMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdLib_RangeObj(self);
    method_returnArg(args, res);
}

void PikaStdLib_StringObjMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdLib_StringObj(self);
    method_returnArg(args, res);
}

void PikaStdLib_SysObjMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdLib_SysObj(self);
    method_returnArg(args, res);
}

PikaObj *New_PikaStdLib(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineConstructor(self, "MemChecker()->any", PikaStdLib_MemCheckerMethod);
#if 0
    class_defineConstructor(self, "RangeObj()->any", PikaStdLib_RangeObjMethod);
#endif
#if 0
    class_defineConstructor(self, "StringObj()->any", PikaStdLib_StringObjMethod);
#endif
    class_defineConstructor(self, "SysObj()->any", PikaStdLib_SysObjMethod);
    return self;
}

void PikaStdLib_MemChecker_getMaxMethod(PikaObj *self, Args *args){
    double res = PikaStdLib_MemChecker_getMax(self);
    method_returnFloat(args, res);
}

void PikaStdLib_MemChecker_getNowMethod(PikaObj *self, Args *args){
    double res = PikaStdLib_MemChecker_getNow(self);
    method_returnFloat(args, res);
}

void PikaStdLib_MemChecker_maxMethod(PikaObj *self, Args *args){
    PikaStdLib_MemChecker_max(self);
}

void PikaStdLib_MemChecker_nowMethod(PikaObj *self, Args *args){
    PikaStdLib_MemChecker_now(self);
}

void PikaStdLib_MemChecker_resetMaxMethod(PikaObj *self, Args *args){
    PikaStdLib_MemChecker_resetMax(self);
}

PikaObj *New_PikaStdLib_MemChecker(Args *args){
    PikaObj *self = New_TinyObj(args);
#if !PIKA_NANO_ENABLE
    class_defineMethod(self, "getMax()->float", PikaStdLib_MemChecker_getMaxMethod);
#endif
#if !PIKA_NANO_ENABLE
    class_defineMethod(self, "getNow()->float", PikaStdLib_MemChecker_getNowMethod);
#endif
    class_defineMethod(self, "max()", PikaStdLib_MemChecker_maxMethod);
    class_defineMethod(self, "now()", PikaStdLib_MemChecker_nowMethod);
#if !PIKA_NANO_ENABLE
    class_defineMethod(self, "resetMax()", PikaStdLib_MemChecker_resetMaxMethod);
#endif
    return self;
}

Arg *PikaStdLib_MemChecker(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdLib_MemChecker);
}
void PikaStdLib_RangeObj___next__Method(PikaObj *self, Args *args){
    Arg* res = PikaStdLib_RangeObj___next__(self);
    method_returnArg(args, res);
}

PikaObj *New_PikaStdLib_RangeObj(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineMethod(self, "__next__()->any", PikaStdLib_RangeObj___next__Method);
    return self;
}

Arg *PikaStdLib_RangeObj(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdLib_RangeObj);
}
void PikaStdLib_StringObj___next__Method(PikaObj *self, Args *args){
    Arg* res = PikaStdLib_StringObj___next__(self);
    method_returnArg(args, res);
}

PikaObj *New_PikaStdLib_StringObj(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineMethod(self, "__next__()->any", PikaStdLib_StringObj___next__Method);
    return self;
}

Arg *PikaStdLib_StringObj(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdLib_StringObj);
}
void PikaStdLib_SysObj___getitem__Method(PikaObj *self, Args *args){
    Arg* obj = args_getArg(args, "obj");
    Arg* key = args_getArg(args, "key");
    Arg* res = PikaStdLib_SysObj___getitem__(self, obj, key);
    method_returnArg(args, res);
}

void PikaStdLib_SysObj___setitem__Method(PikaObj *self, Args *args){
    Arg* obj = args_getArg(args, "obj");
    Arg* key = args_getArg(args, "key");
    Arg* val = args_getArg(args, "val");
    Arg* res = PikaStdLib_SysObj___setitem__(self, obj, key, val);
    method_returnArg(args, res);
}

void PikaStdLib_SysObj___slice__Method(PikaObj *self, Args *args){
    Arg* obj = args_getArg(args, "obj");
    Arg* start = args_getArg(args, "start");
    Arg* end = args_getArg(args, "end");
    int step = args_getInt(args, "step");
    Arg* res = PikaStdLib_SysObj___slice__(self, obj, start, end, step);
    method_returnArg(args, res);
}

void PikaStdLib_SysObj_bytesMethod(PikaObj *self, Args *args){
    Arg* val = args_getArg(args, "val");
    Arg* res = PikaStdLib_SysObj_bytes(self, val);
    method_returnArg(args, res);
}

void PikaStdLib_SysObj_cformatMethod(PikaObj *self, Args *args){
    char* fmt = args_getStr(args, "fmt");
    PikaTuple* var = args_getTuple(args, "var");
    char* res = PikaStdLib_SysObj_cformat(self, fmt, var);
    method_returnStr(args, res);
}

void PikaStdLib_SysObj_chrMethod(PikaObj *self, Args *args){
    int val = args_getInt(args, "val");
    char* res = PikaStdLib_SysObj_chr(self, val);
    method_returnStr(args, res);
}

void PikaStdLib_SysObj_dictMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdLib_SysObj_dict(self);
    method_returnArg(args, res);
}

void PikaStdLib_SysObj_dirMethod(PikaObj *self, Args *args){
    PikaObj* obj = args_getPtr(args, "obj");
    PikaObj* res = PikaStdLib_SysObj_dir(self, obj);
    method_returnObj(args, res);
}

void PikaStdLib_SysObj_execMethod(PikaObj *self, Args *args){
    char* code = args_getStr(args, "code");
    PikaStdLib_SysObj_exec(self, code);
}

void PikaStdLib_SysObj_floatMethod(PikaObj *self, Args *args){
    Arg* arg = args_getArg(args, "arg");
    double res = PikaStdLib_SysObj_float(self, arg);
    method_returnFloat(args, res);
}

void PikaStdLib_SysObj_getattrMethod(PikaObj *self, Args *args){
    PikaObj* obj = args_getPtr(args, "obj");
    char* name = args_getStr(args, "name");
    Arg* res = PikaStdLib_SysObj_getattr(self, obj, name);
    method_returnArg(args, res);
}

void PikaStdLib_SysObj_hexMethod(PikaObj *self, Args *args){
    int val = args_getInt(args, "val");
    char* res = PikaStdLib_SysObj_hex(self, val);
    method_returnStr(args, res);
}

void PikaStdLib_SysObj_idMethod(PikaObj *self, Args *args){
    Arg* obj = args_getArg(args, "obj");
    int res = PikaStdLib_SysObj_id(self, obj);
    method_returnInt(args, res);
}

void PikaStdLib_SysObj_intMethod(PikaObj *self, Args *args){
    Arg* arg = args_getArg(args, "arg");
    int res = PikaStdLib_SysObj_int(self, arg);
    method_returnInt(args, res);
}

void PikaStdLib_SysObj_iterMethod(PikaObj *self, Args *args){
    Arg* arg = args_getArg(args, "arg");
    Arg* res = PikaStdLib_SysObj_iter(self, arg);
    method_returnArg(args, res);
}

void PikaStdLib_SysObj_lenMethod(PikaObj *self, Args *args){
    Arg* arg = args_getArg(args, "arg");
    int res = PikaStdLib_SysObj_len(self, arg);
    method_returnInt(args, res);
}

void PikaStdLib_SysObj_listMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdLib_SysObj_list(self);
    method_returnArg(args, res);
}

void PikaStdLib_SysObj_openMethod(PikaObj *self, Args *args){
    char* path = args_getStr(args, "path");
    char* mode = args_getStr(args, "mode");
    PikaObj* res = PikaStdLib_SysObj_open(self, path, mode);
    method_returnObj(args, res);
}

void PikaStdLib_SysObj_ordMethod(PikaObj *self, Args *args){
    char* val = args_getStr(args, "val");
    int res = PikaStdLib_SysObj_ord(self, val);
    method_returnInt(args, res);
}

void PikaStdLib_SysObj_printMethod(PikaObj *self, Args *args){
    PikaTuple* val = args_getTuple(args, "val");
    PikaStdLib_SysObj_print(self, val);
}

void PikaStdLib_SysObj_printNoEndMethod(PikaObj *self, Args *args){
    Arg* val = args_getArg(args, "val");
    PikaStdLib_SysObj_printNoEnd(self, val);
}

void PikaStdLib_SysObj_rangeMethod(PikaObj *self, Args *args){
    int a1 = args_getInt(args, "a1");
    int a2 = args_getInt(args, "a2");
    Arg* res = PikaStdLib_SysObj_range(self, a1, a2);
    method_returnArg(args, res);
}

void PikaStdLib_SysObj_setattrMethod(PikaObj *self, Args *args){
    PikaObj* obj = args_getPtr(args, "obj");
    char* name = args_getStr(args, "name");
    Arg* val = args_getArg(args, "val");
    PikaStdLib_SysObj_setattr(self, obj, name, val);
}

void PikaStdLib_SysObj_strMethod(PikaObj *self, Args *args){
    Arg* arg = args_getArg(args, "arg");
    char* res = PikaStdLib_SysObj_str(self, arg);
    method_returnStr(args, res);
}

void PikaStdLib_SysObj_typeMethod(PikaObj *self, Args *args){
    Arg* arg = args_getArg(args, "arg");
    Arg* res = PikaStdLib_SysObj_type(self, arg);
    method_returnArg(args, res);
}

PikaObj *New_PikaStdLib_SysObj(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineMethod(self, "__getitem__(obj:any,key:any)->any", PikaStdLib_SysObj___getitem__Method);
    class_defineMethod(self, "__setitem__(obj:any,key:any,val:any)->any", PikaStdLib_SysObj___setitem__Method);
#if PIKA_BUILTIN_STRUCT_ENABLE
    class_defineMethod(self, "__slice__(obj:any,start:any,end:any,step:int)->any", PikaStdLib_SysObj___slice__Method);
#endif
#if !PIKA_NANO_ENABLE
    class_defineMethod(self, "bytes(val:any)->bytes", PikaStdLib_SysObj_bytesMethod);
#endif
#if PIKA_SYNTAX_FORMAT_ENABLE
    class_defineMethod(self, "cformat(fmt:str,*var)->str", PikaStdLib_SysObj_cformatMethod);
#endif
#if !PIKA_NANO_ENABLE
    class_defineMethod(self, "chr(val:int)->str", PikaStdLib_SysObj_chrMethod);
#endif
#if PIKA_BUILTIN_STRUCT_ENABLE
    class_defineMethod(self, "dict()->any", PikaStdLib_SysObj_dictMethod);
#endif
#if !PIKA_NANO_ENABLE
    class_defineMethod(self, "dir(obj:object)->list", PikaStdLib_SysObj_dirMethod);
#endif
#if PIKA_EXEC_ENABLE
    class_defineMethod(self, "exec(code:str)", PikaStdLib_SysObj_execMethod);
#endif
    class_defineMethod(self, "float(arg:any)->float", PikaStdLib_SysObj_floatMethod);
#if !PIKA_NANO_ENABLE
    class_defineMethod(self, "getattr(obj:object,name:str)->any", PikaStdLib_SysObj_getattrMethod);
#endif
#if !PIKA_NANO_ENABLE
    class_defineMethod(self, "hex(val:int)->str", PikaStdLib_SysObj_hexMethod);
#endif
#if !PIKA_NANO_ENABLE
    class_defineMethod(self, "id(obj:any)->int", PikaStdLib_SysObj_idMethod);
#endif
    class_defineMethod(self, "int(arg:any)->int", PikaStdLib_SysObj_intMethod);
    class_defineMethod(self, "iter(arg:any)->any", PikaStdLib_SysObj_iterMethod);
    class_defineMethod(self, "len(arg:any)->int", PikaStdLib_SysObj_lenMethod);
#if PIKA_BUILTIN_STRUCT_ENABLE
    class_defineMethod(self, "list()->any", PikaStdLib_SysObj_listMethod);
#endif
#if PIKA_FILEIO_ENABLE
    class_defineMethod(self, "open(path:str,mode:str)->object", PikaStdLib_SysObj_openMethod);
#endif
#if !PIKA_NANO_ENABLE
    class_defineMethod(self, "ord(val:str)->int", PikaStdLib_SysObj_ordMethod);
#endif
    class_defineMethod(self, "print(*val)", PikaStdLib_SysObj_printMethod);
#if !PIKA_NANO_ENABLE
    class_defineMethod(self, "printNoEnd(val:any)", PikaStdLib_SysObj_printNoEndMethod);
#endif
    class_defineMethod(self, "range(a1:int,a2:int)->any", PikaStdLib_SysObj_rangeMethod);
#if !PIKA_NANO_ENABLE
    class_defineMethod(self, "setattr(obj:object,name:str,val:any)", PikaStdLib_SysObj_setattrMethod);
#endif
    class_defineMethod(self, "str(arg:any)->str", PikaStdLib_SysObj_strMethod);
    class_defineMethod(self, "type(arg:any)->any", PikaStdLib_SysObj_typeMethod);
    return self;
}

Arg *PikaStdLib_SysObj(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdLib_SysObj);
}
void PikaStdTask_TaskMethod(PikaObj *self, Args *args){
    Arg* res = PikaStdTask_Task(self);
    method_returnArg(args, res);
}

PikaObj *New_PikaStdTask(Args *args){
    PikaObj *self = New_TinyObj(args);
    class_defineConstructor(self, "Task()->any", PikaStdTask_TaskMethod);
    return self;
}

void PikaStdTask_Task___init__Method(PikaObj *self, Args *args){
    PikaStdTask_Task___init__(self);
}

void PikaStdTask_Task_call_alwaysMethod(PikaObj *self, Args *args){
    Arg* fun_todo = args_getArg(args, "fun_todo");
    PikaStdTask_Task_call_always(self, fun_todo);
}

void PikaStdTask_Task_call_period_msMethod(PikaObj *self, Args *args){
    Arg* fun_todo = args_getArg(args, "fun_todo");
    int period_ms = args_getInt(args, "period_ms");
    PikaStdTask_Task_call_period_ms(self, fun_todo, period_ms);
}

void PikaStdTask_Task_call_whenMethod(PikaObj *self, Args *args){
    Arg* fun_todo = args_getArg(args, "fun_todo");
    Arg* fun_when = args_getArg(args, "fun_when");
    PikaStdTask_Task_call_when(self, fun_todo, fun_when);
}

void PikaStdTask_Task_platformGetTickMethod(PikaObj *self, Args *args){
    PikaStdTask_Task_platformGetTick(self);
}

void PikaStdTask_Task_run_foreverMethod(PikaObj *self, Args *args){
    PikaStdTask_Task_run_forever(self);
}

void PikaStdTask_Task_run_onceMethod(PikaObj *self, Args *args){
    PikaStdTask_Task_run_once(self);
}

void PikaStdTask_Task_run_until_msMethod(PikaObj *self, Args *args){
    int until_ms = args_getInt(args, "until_ms");
    PikaStdTask_Task_run_until_ms(self, until_ms);
}

PikaObj *New_PikaStdTask_Task(Args *args){
    PikaObj *self = New_PikaStdLib_SysObj(args);
    obj_newObj(self, "calls", "PikaStdData_List", New_PikaStdData_List);
    class_defineMethod(self, "__init__()", PikaStdTask_Task___init__Method);
    class_defineMethod(self, "call_always(fun_todo:any)", PikaStdTask_Task_call_alwaysMethod);
    class_defineMethod(self, "call_period_ms(fun_todo:any,period_ms:int)", PikaStdTask_Task_call_period_msMethod);
    class_defineMethod(self, "call_when(fun_todo:any,fun_when:any)", PikaStdTask_Task_call_whenMethod);
    class_defineMethod(self, "platformGetTick()", PikaStdTask_Task_platformGetTickMethod);
    class_defineMethod(self, "run_forever()", PikaStdTask_Task_run_foreverMethod);
    class_defineMethod(self, "run_once()", PikaStdTask_Task_run_onceMethod);
    class_defineMethod(self, "run_until_ms(until_ms:int)", PikaStdTask_Task_run_until_msMethod);
    return self;
}

Arg *PikaStdTask_Task(PikaObj *self){
    return obj_newObjInPackage(New_PikaStdTask_Task);
}
