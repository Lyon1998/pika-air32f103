/* ******************************** */
/* Warning! Don't modify this file! */
/* ******************************** */
#ifndef __AIR32F1_UART__H
#define __AIR32F1_UART__H
#include <stdio.h>
#include <stdlib.h>
#include "PikaObj.h"

PikaObj *New_AIR32F1_UART(Args *args);

void AIR32F1_UART_platformDisable(PikaObj *self);
void AIR32F1_UART_platformEnable(PikaObj *self);
void AIR32F1_UART_platformGetEventId(PikaObj *self);
void AIR32F1_UART_platformRead(PikaObj *self);
void AIR32F1_UART_platformReadBytes(PikaObj *self);
void AIR32F1_UART_platformWrite(PikaObj *self);
void AIR32F1_UART_platformWriteBytes(PikaObj *self);

#endif
