/* ******************************** */
/* Warning! Don't modify this file! */
/* ******************************** */
#ifndef __AIR32F1_GPIO__H
#define __AIR32F1_GPIO__H
#include <stdio.h>
#include <stdlib.h>
#include "PikaObj.h"

PikaObj *New_AIR32F1_GPIO(Args *args);

void AIR32F1_GPIO_platformDisable(PikaObj *self);
void AIR32F1_GPIO_platformEnable(PikaObj *self);
void AIR32F1_GPIO_platformGetEventId(PikaObj *self);
void AIR32F1_GPIO_platformHigh(PikaObj *self);
void AIR32F1_GPIO_platformLow(PikaObj *self);
void AIR32F1_GPIO_platformRead(PikaObj *self);
void AIR32F1_GPIO_platformSetMode(PikaObj *self);

#endif
