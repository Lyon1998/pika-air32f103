/* ******************************** */
/* Warning! Don't modify this file! */
/* ******************************** */
#ifndef __AIR32F1__H
#define __AIR32F1__H
#include <stdio.h>
#include <stdlib.h>
#include "PikaObj.h"

PikaObj *New_AIR32F1(Args *args);

Arg* AIR32F1_ADC(PikaObj *self);
Arg* AIR32F1_CAN(PikaObj *self);
Arg* AIR32F1_GPIO(PikaObj *self);
Arg* AIR32F1_IIC(PikaObj *self);
Arg* AIR32F1_PWM(PikaObj *self);
Arg* AIR32F1_SPI(PikaObj *self);
Arg* AIR32F1_Time(PikaObj *self);
Arg* AIR32F1_UART(PikaObj *self);

#endif
