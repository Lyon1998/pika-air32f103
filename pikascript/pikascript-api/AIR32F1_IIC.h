/* ******************************** */
/* Warning! Don't modify this file! */
/* ******************************** */
#ifndef __AIR32F1_IIC__H
#define __AIR32F1_IIC__H
#include <stdio.h>
#include <stdlib.h>
#include "PikaObj.h"

PikaObj *New_AIR32F1_IIC(Args *args);

void AIR32F1_IIC_platformDisable(PikaObj *self);
void AIR32F1_IIC_platformEnable(PikaObj *self);
void AIR32F1_IIC_platformGetEventId(PikaObj *self);
void AIR32F1_IIC_platformRead(PikaObj *self);
void AIR32F1_IIC_platformReadBytes(PikaObj *self);
void AIR32F1_IIC_platformWrite(PikaObj *self);
void AIR32F1_IIC_platformWriteBytes(PikaObj *self);

#endif
