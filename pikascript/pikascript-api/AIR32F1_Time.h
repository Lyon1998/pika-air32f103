/* ******************************** */
/* Warning! Don't modify this file! */
/* ******************************** */
#ifndef __AIR32F1_Time__H
#define __AIR32F1_Time__H
#include <stdio.h>
#include <stdlib.h>
#include "PikaObj.h"

PikaObj *New_AIR32F1_Time(Args *args);

void AIR32F1_Time_platformGetEventId(PikaObj *self);
void AIR32F1_Time_platformGetTick(PikaObj *self);
void AIR32F1_Time_sleep_ms(PikaObj *self, int ms);
void AIR32F1_Time_sleep_s(PikaObj *self, int s);

#endif
