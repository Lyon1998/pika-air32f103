/* ******************************** */
/* Warning! Don't modify this file! */
/* ******************************** */
#ifndef __AIR32F1_CAN__H
#define __AIR32F1_CAN__H
#include <stdio.h>
#include <stdlib.h>
#include "PikaObj.h"

PikaObj *New_AIR32F1_CAN(Args *args);

void AIR32F1_CAN_platformDisable(PikaObj *self);
void AIR32F1_CAN_platformEnable(PikaObj *self);
void AIR32F1_CAN_platformGetEventId(PikaObj *self);
void AIR32F1_CAN_platformRead(PikaObj *self);
void AIR32F1_CAN_platformReadBytes(PikaObj *self);
void AIR32F1_CAN_platformWrite(PikaObj *self);
void AIR32F1_CAN_platformWriteBytes(PikaObj *self);

#endif
