/* ******************************** */
/* Warning! Don't modify this file! */
/* ******************************** */
#ifndef __AIR32F1_ADC__H
#define __AIR32F1_ADC__H
#include <stdio.h>
#include <stdlib.h>
#include "PikaObj.h"

PikaObj *New_AIR32F1_ADC(Args *args);

void AIR32F1_ADC_platformDisable(PikaObj *self);
void AIR32F1_ADC_platformEnable(PikaObj *self);
void AIR32F1_ADC_platformGetEventId(PikaObj *self);
void AIR32F1_ADC_platformRead(PikaObj *self);

#endif
